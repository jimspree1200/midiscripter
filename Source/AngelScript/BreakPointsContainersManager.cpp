//
//  BreakPointsContainerManager.cpp
//  MaoJ
//
//  Created by Jim Mazur on 1/5/14.
//
//

#include "BreakPointsContainersManager.h"

BreakPointsContainer* BreakPointsContainersManager::getContainerFor(juce::File file)
{
    for (int i = 0; i < m_breakPointContainers.size(); i++)
    {
        if (m_breakPointContainers[i]->isForSourceCodeFile(file))
            return m_breakPointContainers[i];
    }
    
    m_breakPointContainers.add(new BreakPointsContainer(file));
    return m_breakPointContainers.getLast();
}

bool BreakPointsContainersManager::hasBreakPointContainersFile(juce::File sourceFileDirectory)
{
    jassert(sourceFileDirectory.exists());
    
    if (!sourceFileDirectory.isDirectory())
        sourceFileDirectory = sourceFileDirectory.getParentDirectory();
    
    sourceFileDirectory = sourceFileDirectory.getChildFile("breakpoints.xml");
    
    if (sourceFileDirectory.existsAsFile()) {
        return true;
    }
    
    return false;
}