/*
  ==============================================================================

    AngelScriptEngineBindings.h
    Created: 3 Aug 2014 10:17:07pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef ANGELSCRIPTENGINEBINDINGS_H_INCLUDED
#define ANGELSCRIPTENGINEBINDINGS_H_INCLUDED

#include <string>
#include "../Application.h"
#include "angelscript.h"
#include "AngelScriptEngineLogger.h"

static MidiPortManager* getMidiPortManager()
{
    MidiPortManager* mpm = &MidiScripterApp::getMidiPortManager();
    jassert(mpm != nullptr);
    
    return mpm;
}

void addEventToLogger(std::string &message)
{
    AngelScriptEngineLogger* logger = MidiScripterApp::getAngelScriptEngine().getLogger();
    jassert(logger != nullptr);
    
    logger->addEvent(EventSeverity::Info, String(message));
}

//==============================================================================

class MidiPortHandler {
public:
    static void setMidiDeviceName(const String deviceName)
    {
        const ScopedLock sl(m_deviceNameLock);
        m_deviceName = deviceName;
    }
    
    static String getMidiDeviceName()
    {
        const ScopedLock sl(m_deviceNameLock);
        return m_deviceName;
    }
    
    static void resetMidiDeviceName()
    {
        setMidiDeviceName(String());
    }
    
    static bool isInputPortOpen(String deviceName)
    {
        if (getMidiPortManager()->isMidiInputEnabled(deviceName))
            return true;
        
        MidiScripterApp::getAngelScriptEngine().getLogger()->addEvent(EventSeverity::Warning, String("Could not open "
                                                                                                      + deviceName
                                                                                                      + " Midi input port"                                                           ));
        return false;
    }
    
    static bool isOutputPortOpen(String deviceName)
    {
        if (getMidiPortManager()->isMidiOutputEnabled(deviceName))
            return true;
        
        MidiScripterApp::getAngelScriptEngine().getLogger()->addEvent(EventSeverity::Warning, String("Could not open "
                                                                                                      + deviceName
                                                                                                      + " Midi output port"                                                           ));
        return false;
    }
    
    static bool openMidiInputDeviceOnMessageThread(std::string &deviceName)
    {
        setMidiDeviceName(deviceName);
        
        MessageManager::getInstance()->callFunctionOnMessageThread(openMidiInputDevice, nullptr);
        
        return isInputPortOpen(deviceName);
    }
    
    static void* openMidiInputDevice(void *)
    {
        String portToOpen = getMidiDeviceName();
        resetMidiDeviceName();
        
        if (portToOpen.isNotEmpty())
            getMidiPortManager()->setMidiInputEnabled(portToOpen, true);
        
        return nullptr;
    }
    
    static bool createMidiInputDeviceOnMessageThread(std::string &deviceName)
    {
        setMidiDeviceName(deviceName);
        MessageManager::getInstance()->callFunctionOnMessageThread(createMidiInputDevice, nullptr);
        return isInputPortOpen(deviceName);
    }
    
    static void* createMidiInputDevice(void *)
    {
        String portToOpen = getMidiDeviceName();
        resetMidiDeviceName();
        
        if (portToOpen.isNotEmpty())
            getMidiPortManager()->createVirtualMidiInput(portToOpen);
        
        return nullptr;
    }
    
    static bool openMidiOutputDeviceOnMessageThread(std::string &midiDevice)
    {
        setMidiDeviceName(midiDevice);
        MessageManager::getInstance()->callFunctionOnMessageThread(openMidiOutputDevice, nullptr);
        return isOutputPortOpen(midiDevice);
    }
    
    static void* openMidiOutputDevice(void*)
    {
        String portToOpen = getMidiDeviceName();
        resetMidiDeviceName();
        
        if (portToOpen.isNotEmpty())
            getMidiPortManager()->setMidiOutputEnabled(portToOpen, true);
        return nullptr;
    }
    
    static bool createMidiOutputDeviceOnMessageThread(std::string &midiDevice)
    {
        setMidiDeviceName(midiDevice);
        MessageManager::getInstance()->callFunctionOnMessageThread(createMidiOutputDevice, nullptr);
        return isOutputPortOpen(midiDevice);
    }
    
    static void* createMidiOutputDevice(void*)
    {
        String portToOpen = getMidiDeviceName();
        resetMidiDeviceName();
        
        if (portToOpen.isNotEmpty())
            getMidiPortManager()->createVirtualMidiOutput(portToOpen);
        return nullptr;
    }
    
    static CriticalSection m_deviceNameLock;
    static String m_deviceName;
};

String MidiPortHandler::m_deviceName = String();
CriticalSection MidiPortHandler::m_deviceNameLock;

//==============================================================================

void setMIDIBufferInterval(std::string &midiPort, int millisecondsBetweenMessages)
{
    getMidiPortManager()->setDefaultBufferInterval(midiPort, millisecondsBetweenMessages);
}

void sendMIDIMessage(std::string &midiPort, int byte1, int byte2, int byte3)
{
    const MidiMessage message(byte1, byte2, byte3);
    getMidiPortManager()->sendMidiMessage(midiPort, message, false);
}

void sendMIDIMessageBuffered(std::string &midiPort, int byte1, int byte2, int byte3)
{
    const MidiMessage message(byte1, byte2, byte3);
    getMidiPortManager()->sendMidiMessage(midiPort, message, true);
}

void sendMIDIMessageDelayed(std::string &midiPort, int byte1, int byte2, int byte3, int delayInMs)
{
    const MidiMessage message(byte1, byte2, byte3);
    getMidiPortManager()->sendDelayedMidiMessage(midiPort, message, delayInMs);
}

//==============================================================================

TimerManager* getTimerManager()
{
    TimerManager* tm = MidiScripterApp::getAngelScriptEngine().getTimerManager();
    jassert(tm != nullptr);
    
    return tm;
}

void startTimer(std::string &timerName, int milliseconds, int repeats)
{
    getTimerManager()->startNamedTimer(timerName, milliseconds, repeats);
}

// TODO: Overload into start timer. Use asFUNCTIONPR
void startOneShotTimer(std::string &timerName, int milliseconds)
{
    getTimerManager()->startNamedTimer(timerName, milliseconds, 1);
}

//==============================================================================

void RegisterBindings (asIScriptEngine* engine)
{
    int r;
    r = engine->RegisterGlobalFunction("void DBG(string &in)", asFUNCTION(addEventToLogger), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("bool openMidiInputDevice(string &in)", asFUNCTION(MidiPortHandler::openMidiInputDeviceOnMessageThread), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("bool createMidiInputDevice (string &in)", asFUNCTION(MidiPortHandler::createMidiInputDeviceOnMessageThread), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("bool openMidiOutputDevice(string &in)", asFUNCTION(MidiPortHandler::openMidiOutputDeviceOnMessageThread), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("bool createMidiOutputDevice (string &in)", asFUNCTION(MidiPortHandler::createMidiOutputDeviceOnMessageThread), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("void setMidiBufferInterval(string &in, int)", asFUNCTION(setMIDIBufferInterval), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("void sendMidiMessage(string &in, int, int, int)", asFUNCTION(sendMIDIMessage), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("void sendMidiMessageBuffered(string &in, int, int, int)", asFUNCTION(sendMIDIMessageBuffered), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("void sendMidiMessageDelayed(string &in, int, int, int, int)", asFUNCTION(sendMIDIMessageDelayed), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("void startTimer(string &in, int milliseconds, int repeats)", asFUNCTION(startTimer), asCALL_CDECL);
    jassert( r >= 0 );
    
    r = engine->RegisterGlobalFunction("void startTimer(string &in, int milliseconds)", asFUNCTION(startOneShotTimer), asCALL_CDECL);
    jassert( r >= 0 );
    
    
}

#endif  // ANGELSCRIPTENGINEBINDINGS_H_INCLUDED
