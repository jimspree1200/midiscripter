//
//  BreakPointsContainerManager.h
//  MaoJ
//
//  Created by Jim Mazur on 1/5/14.
//
//

#ifndef __MaoJ__BreakPointsContainersManager__
#define __MaoJ__BreakPointsContainersManager__

#include "JuceHeader.h"
#include "BreakPointsContainer.h"

class BreakPointsContainersManager {
    
public:
    /** Saves all breakpoint containers to XML files. Each directory that contains scripts with breakpoints will have a breakpoint.xml file
     */
    bool saveBreakPointContainersToXML(File xmlFile);
    
    /** Gets a BreakPointContainer for the specified file. In case no existing container exists, a new one will be created.
     */
    BreakPointsContainer* getContainerFor(File file);
    
private:
    
    OwnedArray<BreakPointsContainer> m_breakPointContainers;
    bool hasBreakPointContainersFile(juce::File sourceFileDirectory);
};


#endif /* defined(__MaoJ__BreakPointsContainersManager__) */
