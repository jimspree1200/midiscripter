//
//  AngelScriptDebugger.h
//  MaoJ
//
//  Created by Jim Mazur on 12/15/13.
//
//

#ifndef __MaoJ__AngelScriptDebugger__
#define __MaoJ__AngelScriptDebugger__

#include "../JuceLibraryCode/JuceHeader.h"
#include "angelscript.h"
#include "BreakPointsContainersManager.h"
#include "AngelScriptDebugStackManager.h"

class AngelScriptEngine;

class AngelScriptDebugger : public ChangeBroadcaster
{
public:
    // Constructors
    AngelScriptDebugger(AngelScriptEngine* parent);
    ~AngelScriptDebugger(){};
    
    void reset();
    
    StackManager* getStackManager()                         { return &m_stackManager; };
    BreakPointsContainersManager* getBreakPointsManager()   { return &m_breakpointContainersManager; };

    bool isEnabled()                                        { return m_isEnabled; };
    void setEnabled (bool shouldBeEnabeld);
    
    bool isWaitingForCommand()                              { return m_isWaitingForCommand.get(); };
    
    void LineCallback(asIScriptContext *ctx);
    
    enum DebugAction
	{
        Pause,     // pause at current execution
		Continue,  // continue until next break point
		Step_Into, // stop at next instruction
		Step_Over, // stop at next instruction, skipping called functions
		Step_Out   // run until returning from current function
	};
    
    void setDebuggerAction (DebugAction actionToSet);
    
private:
    AngelScriptEngine* m_parent;
    asIScriptContext *m_context;
    
    bool m_isEnabled = true;
    
    DebugAction m_debugAction = Continue;
    Atomic<int> m_isWaitingForCommand;
    Atomic<int> m_currentCommandAtStackLevel;
    asUINT             m_lastCommandAtStackLevel = 0;
	asIScriptFunction *m_lastFunction;
    
    StackManager m_stackManager;
    
    BreakPointsContainersManager m_breakpointContainersManager;
    
    bool shouldWaitForCommand();
    
    bool checkForBreakPoint();
};

void printStackTree (ValueTree* treeToPrint);

#endif /* defined(__MaoJ__AngelScriptDebugger__) */
