//
//  AngelScriptDebugHelpers.h
//  MaoJ
//
//  Created by James Mazur on 11/9/13.
//
//

#ifndef __MaoJ__AngelScriptDebugHelpers__
#define __MaoJ__AngelScriptDebugHelpers__

#include "../JuceLibraryCode/JuceHeader.h"

class asIScriptContext;

void updateLogger(const String& message);

void logSetArgError(const int& returnError, asIScriptContext* context);

void logExecutionError(const int& returnError, asIScriptContext* context);

#endif /* defined(__MaoJ__AngelScriptDebugHelpers__) */
