//
//  AngelScript.cpp
//  Moaj
//
//  Created by James Mazur on 11/9/13.
//
//

// TODO: Just make one function to initialise, buildscript and start engine

#include "AngelScriptEngine.h"

#include "AngelScriptEngineBindings.h"
#include "angelscript.h"
#include "../../Libaries/sdk/add_on/scriptstdstring/scriptstdstring.h"
#include "../../Libaries/sdk/add_on/scriptbuilder/scriptbuilder.h"
#include "../../Libaries/sdk/add_on/scriptarray/scriptarray.h"
#include "../../Libaries/sdk/add_on/scriptfile/scriptfile.h"
#include "../../Libaries/sdk/add_on/debugger/debugger.h"
#include "AngelScriptDebugHelpers.h"

AngelScriptEngine::AngelScriptEngine()
:
Thread("AngelScript Engine Thread")
{
    m_contextQueue = new ContextQueue(this);
    m_asDebugger = new AngelScriptDebugger(this);
    m_timerManager = new TimerManager(*this);
    
    //m_scriptToBuild = File();
    
    m_engine = nullptr;
    m_engineState = EngineState::notCreated;
    
    m_OS_SupportsVirtualMidi = false;
    #if JUCE_LINUX || JUCE_MAC || JUCE_IOS || DOXYGEN
    m_OS_SupportsVirtualMidi = true;
    #endif
    
    m_functionStartup = nullptr;
    m_functionHandleIncomingMidi = nullptr;
    m_functionTimercallback = nullptr;
    
    createEngine();
    configureEngine();
}

AngelScriptEngine::~AngelScriptEngine()
{
    stopScript();
    stopThread(1000);
    
    // Release the engine
    if (m_engine != nullptr)
    {
        m_engine->Release();
        // Release command doesnt return a null ptr
        m_engine = nullptr;
    }
}

void AngelScriptEngine::run()
{
    while (! threadShouldExit())
    {
        m_contextQueue->executeQueue();
        wait(5000);
    }
}

//==============================================================================

bool AngelScriptEngine::loadScript(const juce::File &script)
{
    jassert(script.existsAsFile());
    m_scriptToBuild = script;
    
    if (m_engineState != EngineState::notCreated)
        stopScript();
    m_logger.addEvent("Script successfully loaded...");
    
    return true;
}

bool AngelScriptEngine::hasScriptLoaded()
{
    return m_scriptToBuild.existsAsFile();
}

bool AngelScriptEngine::runScript()
{
    if (m_engineState == EngineState::BuildError)
    {
        m_logger.addEvent(EventSeverity::Error, "Cannot start script due to Build Errors");
    }
    
    if (m_engineState != EngineState::Ready)
    {
        stopScript();
        buildScript();
        if (m_engineState != EngineState::Ready)
            return false;
    }
    
    // Engine not created, configured properly or script has not been built
    jassert (m_engineState == Ready);
    
    startEngine();
    
    return true;
}

bool AngelScriptEngine::canRunScript()
{
    return (hasScriptLoaded() && m_engineState != EngineState::BuildError);
}

bool AngelScriptEngine::reloadScript()
{
    loadScript(m_scriptToBuild);
    return true;
}

void AngelScriptEngine::handleIncomingMidiMessage(juce::MidiInput *source, const juce::MidiMessage &message)
{
    if (m_engineState != EngineState::Running) return;
    engineHandleIncomingMidiMessage(source, message);
}

bool AngelScriptEngine::startEngine()
{
    startThread(3);
    
    if (!setFunctions())
        return false;
    
    if (!startScript())
        return false;
    
    updateEngineState(EngineState::Running);
    
    return true;
}


bool AngelScriptEngine::stopScript()
{
    // Kill all the timers
    m_timerManager->reset();
    
    // Make sure no MIDI messages arrive
    getMidiPortManager()->disableAllMidiInputs();
    getMidiPortManager()->disableAllMidiOutputs();
    
    // Release any suspended context's in a debug state
    m_asDebugger->reset();
    
    // Release any queued context's that have not executed
    m_contextQueue->reset();
    
    // Kill the engine thread
    stopThread(1000);
    
    m_engine->DiscardModule("MainModule");
    
    m_compiledScripts.clear();
    
    updateEngineState(EngineState::Configured);
    
    return true;
}


bool AngelScriptEngine::createEngine()
{
    jassert(m_engineState == EngineState::notCreated);
    
	// Try to create the script engine
    if (m_engine == nullptr)
        m_engine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
        
	if( m_engine == nullptr )
	{
        m_logger.addEvent(EventSeverity::Error, String("Failed to create engine"));
		return false;
	}
    
    m_logger.addEvent(EventSeverity::Info, String("Engine initialized. Using AngelScript " + (String)ANGELSCRIPT_VERSION_STRING));
    
    m_engine->RegisterGlobalProperty("const bool g_virtualMidiSupport", &m_OS_SupportsVirtualMidi);
    
    m_engineState = EngineState::Created;
    
    return true;
}

void AngelScriptEngine::configureEngine()
{
    jassert(m_engineState = EngineState::Created);
    
    // The script compiler will write any compiler messages to the callback.
    m_engine->SetMessageCallback(asMETHOD(AngelScriptEngine, engineMessageCallback), this, asCALL_THISCALL);
    
    RegisterStdString(m_engine);
    RegisterScriptArray(m_engine, true);
    RegisterScriptFile(m_engine);
    RegisterBindings(m_engine);

   m_engineState = EngineState::Configured;
}

bool AngelScriptEngine::buildScript()
{
    if (m_engineState != Configured)
        stopScript();
    
    jassert(m_engineState = EngineState::Configured);
    
    int checker;
    CScriptBuilder builder;
    
    checker = builder.StartNewModule(m_engine, "MainModule");
    if ( checker < 0 )
    {
        m_logger.addEvent(EventSeverity::Error, "Error while creating new module in engine");
        return false;
    }
    
    checker = builder.AddSectionFromFile(m_scriptToBuild.getFullPathName().toRawUTF8());
    if ( checker < 0 )
    {
        m_logger.addEvent(EventSeverity::Error, "Error while reading script or errors in script itself");
        m_engineState = EngineState::BuildError;
        return false;
    }
    
    checker = builder.BuildModule();
    if ( checker < 0 )
    {
        m_logger.addEvent(EventSeverity::Error, "Error while building module");
        m_engineState = EngineState::BuildError;
        return false;
    }
    
    // Get all the included sections (files) from the builder before it goes out of scope
    m_compiledScripts.clear();
    m_logger.addEvent("Compiled the following scripts:");
    for (int i = 0; i < builder.GetSectionCount(); i++)
    {
        File sectionFile(builder.GetSectionName(i));
        jassert(sectionFile.existsAsFile());
        m_logger.addEvent(sectionFile.getFileName());
        m_compiledScripts.add(sectionFile);
    }
    
    m_logger.addEvent("Script built sucessfully!");
    
    updateEngineState(EngineState::Ready);
    
    return true;
}

bool AngelScriptEngine::canBuildScript()
{
    return (hasScriptLoaded());
}

bool AngelScriptEngine::setFunctions()
{
    asIScriptModule* module = m_engine->GetModule("MainModule");
    m_functionStartup = module->GetFunctionByDecl("void main()");
    if (!m_functionStartup) {
        m_logger.addEvent(EventSeverity::Error, "Could not find function in script: void main()");
        return false;
    }
    
    m_functionHandleIncomingMidi = module->GetFunctionByDecl("void handleIncomingMidiMessage(const string &in, int, int, int)");
    if (!m_functionHandleIncomingMidi) {
        m_logger.addEvent(EventSeverity::Error, "Could not find function in script: void handleIncomingMidiMessage(const string &in, int byte1, int byte2, int byte3)");
        return false;
    }
    
    m_functionTimercallback = module->GetFunctionByDecl("void timerCallback(const string &in)");
    if (!m_functionTimercallback) {
        m_logger.addEvent(EventSeverity::Error, "Could not find function in script: void timerCallback(const string &in)");
        return false;
    }
    
    return true;
}

bool AngelScriptEngine::startScript()
{
    asIScriptContext* context = m_contextQueue->createContext();
    
    context->Prepare(m_functionStartup);
    
    notify();
    
    m_logger.addEvent("Script Started");
    
    return true;
}

void AngelScriptEngine::updateEngineState(const AngelScriptEngine::EngineState &state)
{
    m_engineState = state;
    ApplicationCommandManager* cm = &MidiScripterApp::getCommandManager();
    if (cm != nullptr);
        sendChangeMessage();
}

//==============================================================================

void AngelScriptEngine::engineHandleIncomingMidiMessage(juce::MidiInput *source, const juce::MidiMessage &message)
{
    // Source name MUST be static because the context will reference the string by address
    static std::string sourceName;
    
    // Convert source to string
    sourceName = source->getName().toStdString();
    
    // Extract 3 bytes from MIDI message. Non-existant bytes will remain as 0;
    uint8 bytes[] = { 0, 0, 0 };
    for (int i = 0; i < message.getRawDataSize(); i++) {
        bytes[i] = *(message.getRawData()+i);
    }
    
    asIScriptContext* context = m_contextQueue->createContext();
    context->Prepare(m_functionHandleIncomingMidi);
    
    int debugValue;
    
    // Setup device name string argument
    debugValue = context->SetArgAddress(0, &sourceName);
    if (debugValue < 0) {
        logSetArgError(debugValue, context);
        context->Release();
        return;
    }
    
    // Setup 3 arguments used in script for the 3 bytes
    for (int i = 0; i < 3; i++) {
        // Set singlular argument
        debugValue = context->SetArgDWord(i+1, bytes[i]);
        // Catch failure in setArg
        if (debugValue < 0)
        {
            logSetArgError(debugValue, context);
            context->Release();
            return;
        }
    }
    
    notify();
}

void AngelScriptEngine::engineTimerCallback(const juce::String &timerName)
{
    std::string cstringName = timerName.toStdString();
    
    asIScriptContext* context = m_contextQueue->createContext();
    context->Prepare(m_functionTimercallback);
    
    int debugValue;
    
    // Setup timer name string argument
    debugValue = context->SetArgAddress(0, &cstringName);
    if (debugValue < 0) {
        logSetArgError(debugValue, context);
        context->Unprepare();
        return;
    }
    
    notify();
    
}

void AngelScriptEngine::engineMessageCallback(const asSMessageInfo *msg)
{
    juce::String engineMessage;
    engineMessage = String(msg->section) + " (" + String(msg->row) + ", " + String(msg->col) + ") : " + msg->message;
    
    if (msg->type == asMSGTYPE_ERROR)
        m_logger.addEvent(EventSeverity::Error, String(engineMessage));
    else if (msg->type == asMSGTYPE_WARNING)
        m_logger.addEvent(EventSeverity::Warning, String(engineMessage));
    else
        m_logger.addEvent(EventSeverity::Info, String(engineMessage));
}