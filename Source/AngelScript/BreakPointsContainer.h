//
//  BreakPointsContainer.h
//  MaoJ
//
//  Created by Jim Mazur on 1/5/14.
//
//

#ifndef __MaoJ__BreakPointsContainer__
#define __MaoJ__BreakPointsContainer__

#include "JuceHeader.h"

/** This object contains breakpoints loosely associated with a file.
 Its possible to extract the breakpoints as a ValueTree or XML document
 for use in other parts of the code.
 
 Reloading of breakpoints can be achieved via the setFromXML function.
 */

class BreakPointsContainer {
    
public:
    BreakPointsContainer(File sourceCodeDocumet);
    
    /** Returns the source code File that was given during construction.
     */
    File getSourceCodeFile()                                  { return sourceCode; };
    
    /** Returns true if the file that this breakpoint container is for matches the specified file
     */
    bool isForSourceCodeFile ( File file);
    
    /** Adds a breakpoint at the specified line number.
     
     In case a breakpoint already exsists at the line number,
     this funcion does nothing.
     */
    void addBreakpoint (int lineNumber, bool enabled = true);
    
    /** Removes the breakpoint at the specified line number.
     */
    void removeBreakpoint (int lineNumber);
    
    /** Returns if a breakpoint exsits at the specified line number.
     */
    bool hasBreakPoint (int lineNumber);
    
    /** Returns if a breakpoint is active at the specified line number.
     */
    bool breakpointIsActive (int lineNumber);
    
    /** Toggles the breakpoint as active or inactive.
     In case the breakpoint does not exist, this function will create it.
     */
    bool toggleBreakPoint (int lineNumber);
    
    /** Returns the reference to the ValueTree that represents the breakpoints.
     */
    ValueTree& getBreakpointsTree()                           { return (*breakpoints); };
    
    /** Returns the Breakpoints as an XmlElement
     */
    XmlElement getBreakpointsAsXML();
    
    /** Restores the Breakpoints from an XmlDocument
     */
    void setBreakpointsFromXML (XmlDocument* doc);
    
private:
    File sourceCode;
    
    ScopedPointer<ValueTree> breakpoints;
    
    ValueTree findBreakPoint(int lineNumber);
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BreakPointsContainer)
};


#endif /* defined(__MaoJ__BreakPointsContainer__) */
