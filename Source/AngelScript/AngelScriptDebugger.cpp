//
//  AngelScriptDebugger.cpp
//  MaoJ
//
//  Created by Jim Mazur on 12/15/13.
//
//

#include "AngelScriptDebugger.h"
#include "AngelScriptEngine.h"
#include "../Application.h"

//==============================================================================

AngelScriptDebugger::AngelScriptDebugger(AngelScriptEngine* engine)
{
    m_parent = engine;
    m_context = nullptr;
    
    m_isWaitingForCommand.set(false);
}

void AngelScriptDebugger::reset()
{
    if (m_context != nullptr) {
        m_context->Abort();
    }
    m_isWaitingForCommand.set(false);
}

void AngelScriptDebugger::setEnabled(bool shouldBeEnabeld)
{
    m_isEnabled = shouldBeEnabeld;
    sendChangeMessage();
}

void AngelScriptDebugger::LineCallback(asIScriptContext *ctx)
{
    if (!isEnabled())
        return;
    m_context = ctx;
    // Get current callstack size incase step over / out is used
    m_currentCommandAtStackLevel.set((int) ctx->GetCallstackSize());
    
    if (shouldWaitForCommand())
    {
        m_isWaitingForCommand.set(true);
        
        // There might be a better place for this
        MidiScripterApp::getCommandManager().commandStatusChanged();
        
        m_stackManager.updateStack(ctx);
        
        while (m_isWaitingForCommand.get() && !m_parent->threadShouldExit())
            m_parent->sleep(300);
    }
}

void AngelScriptDebugger::setDebuggerAction(AngelScriptDebugger::DebugAction actionToSet)
{
    // Probably haven't started debugging
    if (!isEnabled() && m_debugAction == Continue && actionToSet == Pause)
        setEnabled(true);
    
    m_debugAction = actionToSet;
    
    if (actionToSet == Step_Over || actionToSet == Step_Out)
    {
        m_lastCommandAtStackLevel = m_currentCommandAtStackLevel.get();
    }
    
    m_isWaitingForCommand.set(false);
}

bool AngelScriptDebugger::shouldWaitForCommand ()
{
    // By default we ignore callbacks when the context is not active.
	// An application might override this to for example disconnect the
	// debugger as the execution finished.
	if( m_context->GetState() != asEXECUTION_ACTIVE )
		return false;
    
    if ( m_debugAction == Pause )
    {
        return true;
    }
    
	if( m_debugAction == Continue )
	{
		if( !checkForBreakPoint() )
			return false;
	}
	else if( m_debugAction == Step_Over )
	{
		if( m_context->GetCallstackSize() > m_lastCommandAtStackLevel )
		{
			if( !checkForBreakPoint() )
				return false;
		}
	}
	else if( m_debugAction == Step_Out )
	{
		if( m_context->GetCallstackSize() >= m_lastCommandAtStackLevel )
		{
			if( !checkForBreakPoint() )
				return false;
		}
	}
	else if( m_debugAction == Step_Into )
	{
		checkForBreakPoint();
        
		// Always break, but we call the check break point anyway
		// to tell user when break point has been reached
	}
    
    return true;
    
}

bool AngelScriptDebugger::checkForBreakPoint()
{
    File section;
    int lineNumber;
    
    {
        // Get filename (stored as section name during compilation of the script)
        const char *temp = 0;
        lineNumber = m_context->GetLineNumber(0,0, &temp);
        section = temp;
    }
    
    BreakPointsContainer *sectionBreakpointContainer = m_breakpointContainersManager.getContainerFor(section);
    
    if (sectionBreakpointContainer->hasBreakPoint(lineNumber))
        return true;
    
    return false;
}
