//
//  AngelScriptDebugHelpers.cpp
//  MaoJ
//
//  Created by James Mazur on 11/9/13.
//
//

#include "AngelScriptDebugHelpers.h"
#include "../Application.h"

void updateLogger(const String& message)
{
    AngelScriptEngineLogger* logger = MidiScripterApp::getAngelScriptEngine().getLogger();
    logger->addEvent(EventSeverity::Error, message);
}

void logSetArgError(const int& returnError, asIScriptContext* context)
{
    String description = "Unknown Error";
    String function = context->GetFunction()->GetName();
    switch (returnError) {
        case asCONTEXT_NOT_PREPARED:
            description = function + " - Execution failed: The context is not prepared or is not in a suspended state";
            break;
            
        case asINVALID_ARG:
            description = function + " - The arg is larger than the number of arguments in the prepared function.";
            break;
            
        case asINVALID_TYPE:
            description = function + "The argument is not the correct type";
            break;
            
        default:
            break;
    }
    updateLogger(description);
}

void logExecutionError(const int& returnError, asIScriptContext* context)
{
    String description = "Unknown Error";
    
    switch (returnError) {
        case asCONTEXT_NOT_PREPARED:
            description = "Execution failed: The context is not prepared or is not in a suspended state";
            break;
            
        case asEXECUTION_ABORTED:
            description = "Execution failed: Was aborted with a call to abort";
            break;
            
        case asEXECUTION_SUSPENDED:
            description = "Execution failed: The execution was suspended with a call to Suspend";
            break;
            
        case asEXECUTION_EXCEPTION:
        {
            asIScriptFunction *func = context->GetExceptionFunction();
            description = "Execution failed: The execution ended with an exception." + newLine +
            "function:         " + func->GetDeclaration() + newLine +
            "module:           " + func->GetModuleName() + newLine +
            "script section:   " + func->GetScriptSectionName() + newLine +
            "line:             " + String(context->GetExceptionLineNumber()) + newLine +
            "description       " + context->GetExceptionString();
            break;
        }
            
        default:
            break;
    }
    
    updateLogger(description);
}

