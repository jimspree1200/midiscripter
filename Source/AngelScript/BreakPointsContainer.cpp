//
//  BreakPointsContainer.cpp
//  MaoJ
//
//  Created by Jim Mazur on 1/5/14.
//
//

#include "BreakPointsContainer.h"

BreakPointsContainer::BreakPointsContainer(File sourceCodeDocument)
:
sourceCode(sourceCodeDocument)
{
    breakpoints = new ValueTree("Breakpoints");
    breakpoints->setProperty("FileName", sourceCodeDocument.getFullPathName(), nullptr);
}

bool BreakPointsContainer::isForSourceCodeFile(juce::File file)
{
    if (sourceCode == file)
        return true;
    return false;
}

void BreakPointsContainer::addBreakpoint(int lineNumber, bool enabled)
{
    if (hasBreakPoint(lineNumber))
        return;
    
    ValueTree breakpoint("Breakpoint");
    breakpoint.setProperty("Line", lineNumber, nullptr);
    breakpoint.setProperty("Enabled", enabled, nullptr);
    
    breakpoints->addChild(breakpoint, -1, nullptr);
}

void BreakPointsContainer::removeBreakpoint(int lineNumber)
{
    ValueTree breakpoint = findBreakPoint(lineNumber);
    breakpoints->removeChild(breakpoint, nullptr);
}

bool BreakPointsContainer::hasBreakPoint(int lineNumber)
{
    ValueTree breakpoint = findBreakPoint(lineNumber);
    if (breakpoint.isValid()) {
        return true;
    }
    return false;
}

bool BreakPointsContainer::breakpointIsActive(int lineNumber)
{
    ValueTree breakpoint = findBreakPoint(lineNumber);
    return (bool) breakpoint.getProperty("Enabled");
    
    return false;
}

bool BreakPointsContainer::toggleBreakPoint(int lineNumber)
{
    if (!hasBreakPoint(lineNumber)) {
        addBreakpoint(lineNumber);
        return true;
    }
    
    ValueTree breakpoint = findBreakPoint(lineNumber);
    if ((bool) breakpoint.getProperty("Enabled") == true)
    {
        breakpoint.setProperty("Enabled", false, nullptr);
        return false;
    }
    else
    {
        breakpoint.setProperty("Enabled", true, nullptr);
        return true;
    }
}

XmlElement BreakPointsContainer::getBreakpointsAsXML()
{
    ScopedPointer<XmlElement> xmlElement;
    xmlElement = (breakpoints->createXml());
    return *xmlElement;
}

void BreakPointsContainer::setBreakpointsFromXML(juce::XmlDocument *doc)
{
    XmlElement* element = doc->getDocumentElement();
    jassert(element != nullptr);
    
    breakpoints->fromXml(*element);
    sourceCode = File(breakpoints->getProperty("FileName").toString());
}

ValueTree BreakPointsContainer::findBreakPoint(int lineNumber)
{
    int childCount = breakpoints->getNumChildren();
    
    if (childCount > 0) {
        for (int i = 0; i < childCount; i++)
        {
            if ((int) breakpoints->getChild(i).getProperty("Line") == lineNumber)
                return breakpoints->getChild(i);
        }
    }
    
    return ValueTree();
}
