/*
  ==============================================================================

    AngelScriptDebugStackManager.h
    Created: 7 Aug 2014 4:20:56pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef ANGELSCRIPTDEBUGSTACKMANAGER_H_INCLUDED
#define ANGELSCRIPTDEBUGSTACKMANAGER_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class asIScriptContext;

class StackManager : public ChangeBroadcaster
{
public:
    StackManager();
    
    void updateStack(asIScriptContext *ctx);
    
    ValueTree getStackTree();
    
private:
    CriticalSection m_stackTreeLock;
    ScopedPointer<ValueTree> m_stackTree;
    ScopedPointer<ValueTree> m_stackTreeForGUI;
    
    ValueTree updateStackLevel(asIScriptContext *ctx, int stackLevel);
    ValueTree createEmptyVariableTree();
    void setVariableName(ValueTree variableTree, const String& name);
    void setVariableTypeId(ValueTree variableTree, const int& typeId, const String& typeName = String());
    void setVariableValue(ValueTree variableTree, const String& value);
    void setVariableScope(ValueTree variableTree, const bool& isInScope);
    void setVariableIsObjHandle(ValueTree variableTree, const bool& isObjHandle);
    void setVariableAddress(ValueTree variableTree, const String& address);
    
    bool isFundementalType(const int &TypeId);
    String fundementalTypeToString(const void* variable, const int &typeToCastFrom);
    
    String getAddressOf(const void* obj);
    
    void handleEngineObject(const void* obj, const int &typeId, ValueTree stackVariables, ValueTree variableTree);
    void handleFundementalTypes(const void* obj, const int &typeId, ValueTree variableTree);
    void handleNonFundementalTypes(const void* obj, const int &typeId, ValueTree stackVariables, ValueTree variableTree);
    void handleObjHandle(const void* obj, const int &typeId, ValueTree stackVariables, ValueTree variableTree);
    void handleScriptObj(const void* obj, const int &typeId, ValueTree stackVariables, ValueTree variableTree);
    void handleAppObj(const void* obj, const int &typeId, ValueTree variableTree);
    void handleTemplate(const void* obj, const int &typeId, ValueTree variableTree);

};



#endif  // ANGELSCRIPTDEBUGSTACKMANAGER_H_INCLUDED
