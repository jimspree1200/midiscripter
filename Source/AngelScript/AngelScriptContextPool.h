//
//  AngelScriptContextPool.h
//  MaoJ
//
//  Created by Jim Mazur on 12/18/13.
//
//

#ifndef __MaoJ__AngelScriptContextPool__
#define __MaoJ__AngelScriptContextPool__

#include <iostream>
#include <vector>
#include <queue>
#include "../JuceLibraryCode/JuceHeader.h"
#include "../../Libaries/sdk/angelscript/include/angelscript.h"

class AngelScriptEngine;

//==============================================================================
/**
 A pool of execution contexts for an AngelScript engine. You can get a context
 from the pool and then return it when done. As the contexts are not lightweight
 object, the pool keeps too many contexts from being prepared.
 */

class ContextPool {
    
public:
    //==============================================================================
    /** Creates a new context pool */
    ContextPool(AngelScriptEngine* owner);
    
    /** Get a context from the pool. In case a context is not already available 
     a new context will be created. Once done with the context, return it using
     the returnContextToPool.
     */
    asIScriptContext* getContextFromPool();
    
    /** Returns the context to the pool. Use this when context is done being
     executed.
     */
    void returnContextToPool(asIScriptContext* ctx);
    
    /** Resets the pools in the Context.
     */
    void reset();
    
private:
    AngelScriptEngine* m_owner;
    // The pool of contexts that are prepared for use
    std::vector<asIScriptContext*> m_pool;
};

//==============================================================================
/**
 A queue of contexts that is threadsafe. Use createContext to get a context.
 Once ready to execute the queue, call the executeQueue function.
 */

class ContextQueue
{
public:
    /** Initialise the ContextQueue by connecting it with the AngelScriptEngine
     */
    ContextQueue(AngelScriptEngine* owner);
    
    /** Upon destruction of the ContextQueue, it will suspend all queued up
     contexts.
     */
    ~ContextQueue();

    /** Get a context for execution.
     */
    asIScriptContext* createContext();
    
    /** Execute the queue.
     */
    bool executeQueue();
    
    /** Get the critical section.
     */
    CriticalSection& getLock() { return m_lock; };
    
    /** Reset all contexts.
     */
    void reset();
    
private:
    //==============================================================================
    AngelScriptEngine* m_parent;
    std::queue<asIScriptContext*> m_queue;
    ContextPool m_pool;
    CriticalSection m_lock;
};

#endif /* defined(__MaoJ__AngelScriptContextPool__) */
