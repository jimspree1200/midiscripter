//
//  AngelScriptContextPool.cpp
//  MaoJ
//
//  Created by Jim Mazur on 12/18/13.
//
//

#include "AngelScriptContextPool.h"
#include "AngelScriptEngine.h"
#include "AngelScriptDebugHelpers.h"

ContextPool::ContextPool(AngelScriptEngine* owner)
:
m_owner(owner)
{
    
}

asIScriptContext* ContextPool::getContextFromPool()
{
    asIScriptContext* contextToReturn = nullptr;
    
    if (m_pool.size()) {
        contextToReturn = *m_pool.rbegin();
        m_pool.pop_back();
    }
    else
        contextToReturn = m_owner->m_engine->CreateContext();
    
    return contextToReturn;
}

void ContextPool::returnContextToPool(asIScriptContext *ctx)
{
    m_pool.push_back(ctx);
}

void ContextPool::reset()
{
    m_pool.clear();
}

ContextQueue::ContextQueue(AngelScriptEngine* parent)
:
m_parent(parent),
m_pool(parent)
{
}

ContextQueue::~ContextQueue()
{
    while (!m_queue.empty())
    {
        const ScopedLock sl(m_lock);
        asIScriptContext* context = m_queue.front();
        m_queue.pop();
        context->Suspend();
    }
}

asIScriptContext* ContextQueue::createContext()
{
    const ScopedLock sl(m_lock);
    asIScriptContext* newContext = m_pool.getContextFromPool();
    m_queue.push(newContext);
    return newContext;
}

bool ContextQueue::executeQueue()
{
    
    while (!m_queue.empty()) {
        // Get the context to process
        asIScriptContext* context;
        {
            const ScopedLock sl(m_lock);
            context = m_queue.front();
            // Pull it from the queue
            m_queue.pop();
        }
        
        // Check if context should callback to the debugger
        if (m_parent->getDebugger()->isEnabled())
            context->SetLineCallback(asMETHOD(AngelScriptDebugger, LineCallback), m_parent->m_asDebugger, asCALL_THISCALL);
        
        // Try to execute the context
        const int debugValue = context->Execute();
        if (debugValue != asEXECUTION_FINISHED && !m_parent->threadShouldExit())
            logExecutionError(debugValue, context);
        
        // Unprepare the context so it can be prepared by the next caller
        context->Unprepare();
        {
            const ScopedLock sl(m_lock);
            m_pool.returnContextToPool(context);
        }
    }
    return true;
}

void ContextQueue::reset()
{
    const ScopedLock sl(m_lock);
    m_pool.reset();
}