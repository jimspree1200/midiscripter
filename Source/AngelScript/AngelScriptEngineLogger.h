/*
  ==============================================================================

    AngelScriptEngineLogger.h
    Created: 29 Jul 2014 11:35:59am
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef ANGELSCRIPTENGINELOGGER_H_INCLUDED
#define ANGELSCRIPTENGINELOGGER_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/**
Different severity levels for the event.
 */
enum EventSeverity
{
    Info,
    Warning,
    Error
};

//==============================================================================
/**
 An Event to be stored in the AngelScriptEngineLogger. Each event contains a 
 timestamp, severity level (info, warning, error) and a message that can be stored
 */

class AngelScriptEngineEvent
{
public:
    //==============================================================================
    /** Default constructor, uses default system time, severity of info and no 
     message.
     */
    AngelScriptEngineEvent();
    
    /** Constructs an event with the specified timestamp, severity level and message
    */
    AngelScriptEngineEvent(Time timeStamp, EventSeverity eventType, String message);
    
    /** Copy constructor and assignment operator. */
    AngelScriptEngineEvent(const AngelScriptEngineEvent&);
    AngelScriptEngineEvent& operator=( const AngelScriptEngineEvent& other);
    
    /** Get the time of the event. */
    Time getTime()                  { return m_timeStamp; };
    
    /** Get the severity of the event. */
    EventSeverity getSeverity()     { return m_eventSeverity; };
    
    /** Get the message of the event. */
    String getMessage()             { return m_eventMessage; };
    
private:
    //==============================================================================
    Time m_timeStamp;
    EventSeverity m_eventSeverity;
    String m_eventMessage;
};

//==============================================================================
/**
 A logger for the AngelScriptEngine. Simply add events to the enginve by using
 the addEvent function.
 
 To receive events from the logger in an object, simply derive the object using
 the AngelScriptEngineEventListner object.
 */

class AngelScriptEngineLogger : public ChangeBroadcaster
{
public:
    //==============================================================================
    /** Add a new event to the logger. */
    void addEvent (EventSeverity eventType, String message);
    
    /** Add a new event to the logger as an info. */
    void addEvent (String message);
    
    /** Get all events in the logger. */
    Array<AngelScriptEngineEvent> getAllEvents();

    /** Get events since a certain timestamp. */
    Array<AngelScriptEngineEvent> getEventsSince (Time time);
    
    /** Clear all the events in the logger. */
    void reset();
    
private:
    //==============================================================================
    CriticalSection lock;
    Array<AngelScriptEngineEvent> m_storedEvents;
    uint16 m_numOfEventsToKeep = 1000;
    
    void checkIfStoredEventsAreFull();
};

//==============================================================================
/**
Receives updates from the event listener. Create one of these by initializing it
 with a pointer to the logger to recieve events from. Upon creation it will get
 all events in the log.
*/

class AngelScriptEngineEventListener : public ChangeListener
{
public:
    //==============================================================================
    /** Instantiate the AngelScriptEngineEventListner with a pointer to the logger
     it should receive events from. */
    AngelScriptEngineEventListener(AngelScriptEngineLogger* logger);
    
    ~AngelScriptEngineEventListener();

    /** Override this method to handle receiving new events from the logger 
     */
    virtual void handleNewEvent(AngelScriptEngineEvent event) = 0;
    
    /** Call back from the AngelScriptEngineLogger
     */
    void changeListenerCallback(ChangeBroadcaster *source);
    
private:
    //==============================================================================
    AngelScriptEngineLogger* m_logger;
    Time m_time;
};

#endif  // ANGELSCRIPTENGINELOGGER_H_INCLUDED
