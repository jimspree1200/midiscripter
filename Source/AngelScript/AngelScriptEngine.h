//
//  AngelScript.h
//  Moaj
//
//  Created by James Mazur on 11/9/13.
//
//

#ifndef __Moaj__AngelScript__
#define __Moaj__AngelScript__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

#include "AngelScriptContextPool.h"
#include "AngelScriptDebugger.h"
#include "../Controller/MidiPortManager.h"
#include "../Controller/TimerManager.h"
#include "AngelScriptEngineLogger.h"

class MidiPortController;

class AngelScriptEngine : public MidiInputCallback,
                          public Thread,
                          public ChangeBroadcaster
{
public:
    AngelScriptEngine();
    ~AngelScriptEngine();
    
    bool loadScript (const File &script);
    bool hasScriptLoaded ();
    
    bool buildScript();
    bool canBuildScript();
    
    // Runs the script specified in the scriptToBuild file
    bool runScript ();
    bool canRunScript();
    
    // This reloads the script previously built using the runScript command
    bool reloadScript();
    
    Array<File> getCompiledScripts()                            { return m_compiledScripts; };
    
    //==============================================================================
    
    // This will pass along the MIDI message and source to the script via the  handleIncomingMidiMessage(std::string, int byte, int byte, int byte) function
    void handleIncomingMidiMessage(juce::MidiInput *source, const juce::MidiMessage &message);
    
    //==============================================================================
    
    AngelScriptDebugger* getDebugger()                          { return m_asDebugger; };
    
    //==============================================================================
    
    enum EngineState
    {
        // No engine created
        notCreated,
        
        // Engine has been created but not configured
        Created,
        
        // Engine has been configured
        Configured,
        
        // Script failed while building
        BuildError,
        
        // Script has been built and is ready for contexts
        Ready,
        
        // Engine is running
        Running
    };
    
    //==============================================================================
    
    EngineState getEngineState()                                { return m_engineState; };
    
    AngelScriptEngineLogger* getLogger()                        { return &m_logger; };
    
    TimerManager* getTimerManager()                             { return m_timerManager; };

private:
    asIScriptEngine *m_engine;
    EngineState m_engineState;
    bool m_OS_SupportsVirtualMidi;
    
    friend class ContextQueue;
    friend class ContextPool;
    ScopedPointer<ContextQueue> m_contextQueue;
    
    File m_scriptToBuild;
    Array<File> m_compiledScripts;
    
    AngelScriptEngineLogger m_logger;
    
    ScopedPointer<AngelScriptDebugger> m_asDebugger;
    
    friend TimerManager;
    ScopedPointer<TimerManager> m_timerManager;
    
    asIScriptFunction *m_functionStartup = nullptr;
    asIScriptFunction *m_functionHandleIncomingMidi = nullptr;
    asIScriptFunction *m_functionTimercallback = nullptr;
    
    // Engine setup
    bool createEngine();
    void configureEngine();
    
    bool startEngine();
    bool setFunctions();
    bool startScript();
    bool stopScript();
    
    void updateEngineState(const EngineState &state);
    
    void run();
    
    // Functions into the engine
    void engineHandleIncomingMidiMessage(juce::MidiInput *source, const juce::MidiMessage &message);
    void engineTimerCallback(const String &timerName);
    
    // API's from the engine
    void engineMessageCallback(const asSMessageInfo *msg);
    
};

#endif /* defined(__Moaj__AngelScript__) */
