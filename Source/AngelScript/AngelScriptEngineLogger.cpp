/*
  ==============================================================================

    AngelScriptEngineLogger.cpp
    Created: 29 Jul 2014 11:35:59am
    Author:  Jim Mazur

  ==============================================================================
*/

#include "AngelScriptEngineLogger.h"
#include "AngelScriptEngine.h"

AngelScriptEngineEvent::AngelScriptEngineEvent()
{
    
}

AngelScriptEngineEvent::AngelScriptEngineEvent(Time timeStamp, EventSeverity eventType, String message)
:
m_timeStamp(timeStamp),
m_eventSeverity(eventType),
m_eventMessage(message)
{
    
}

AngelScriptEngineEvent::AngelScriptEngineEvent(const AngelScriptEngineEvent& other)
{
    m_timeStamp = other.m_timeStamp;
    m_eventSeverity = other.m_eventSeverity;
    m_eventMessage = other.m_eventMessage;
}

AngelScriptEngineEvent& AngelScriptEngineEvent::operator=(const AngelScriptEngineEvent &other)
{
    m_timeStamp = other.m_timeStamp;
    m_eventSeverity = other.m_eventSeverity;
    m_eventMessage = other.m_eventMessage;
    return *this;
}

void AngelScriptEngineLogger::addEvent(EventSeverity eventType, juce::String message)
{
    const ScopedLock sl (lock);
    checkIfStoredEventsAreFull();
    m_storedEvents.add(AngelScriptEngineEvent(Time::getCurrentTime(), eventType, message));
    DBG("Added message: " + message);
    sendChangeMessage();
}

void AngelScriptEngineLogger::addEvent(juce::String message)
{
    const ScopedLock sl (lock);
    checkIfStoredEventsAreFull();
    m_storedEvents.add(AngelScriptEngineEvent(Time::getCurrentTime(), EventSeverity::Info, message));
    DBG("Added message: " + message);
    sendChangeMessage();
}

Array<AngelScriptEngineEvent> AngelScriptEngineLogger::getAllEvents()
{
    const ScopedLock sl (lock);
    Array<AngelScriptEngineEvent> events = m_storedEvents;
    return events;
}

Array<AngelScriptEngineEvent> AngelScriptEngineLogger::getEventsSince(juce::Time time)
{
    Array<AngelScriptEngineEvent> events;
    
    const ScopedLock sl (lock);
    for (int i = m_storedEvents.size()-1; i >= 0; i--) {
        if (m_storedEvents[i].getTime() <= time)
        {
            break;
        }
        events.insert(0, m_storedEvents[i]);
    }
    
    return events;
}

void AngelScriptEngineLogger::reset()
{
    const ScopedLock sl (lock);
    m_storedEvents.clear();
}

void AngelScriptEngineLogger::checkIfStoredEventsAreFull()
{
    if (m_storedEvents.size() >= m_numOfEventsToKeep)
    {
        m_storedEvents.removeRange(m_numOfEventsToKeep - 1, m_storedEvents.size() - m_numOfEventsToKeep + 1);
    }
}

AngelScriptEngineEventListener::AngelScriptEngineEventListener(AngelScriptEngineLogger* logger)
{
    m_logger = logger;
    m_logger->addChangeListener(this);
    m_logger->sendChangeMessage();
}

AngelScriptEngineEventListener::~AngelScriptEngineEventListener()
{
    m_logger->removeChangeListener(this);
}

void AngelScriptEngineEventListener::changeListenerCallback(juce::ChangeBroadcaster *source)
{
    if (source != m_logger) return;
    
    Array<AngelScriptEngineEvent> events;
    
    // Check to see if this was the first time this object is receiving the callback. When constructed the object will default to standard Time() value
    if (m_time == Time())
        events = m_logger->getAllEvents();
    else
        events = m_logger->getEventsSince(m_time);
    
    m_time = Time::getCurrentTime();
    
    for (auto event : events)
        handleNewEvent(event);
}