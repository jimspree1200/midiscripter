/*
  ==============================================================================

    AngelScriptDebugStackManager.cpp
    Created: 7 Aug 2014 4:20:56pm
    Author:  Jim Mazur

  ==============================================================================
*/

#include "AngelScriptDebugStackManager.h"

#include "../../Libaries/sdk/add_on/scriptarray/scriptarray.h"
#include <sstream> // stringstream

// TODO: Debugger should also know about all files that created as sections after the script is complied, therefore the GUI can also load the active script sections for displaying breakpoints, current function, etc

// TODO: Find some threadsafe method of working with the ValueTree's, it could happen that the debugger updates the tree while the UI thread is redrawing!

String typeIDToString(const int typeId)
{
    if (typeId == asTYPEID_VOID)                return "Void";
    else if( typeId == asTYPEID_BOOL )          return "Bool";
	else if( typeId == asTYPEID_INT8 )          return "Int8";
	else if( typeId == asTYPEID_INT16 )         return "Int";
	else if( typeId == asTYPEID_INT32 )         return "Int32";
	else if( typeId == asTYPEID_INT64 )         return "Int64";
    else if( typeId == asTYPEID_UINT8 )         return "UInt16";
	else if( typeId == asTYPEID_UINT16 )        return "UInt";
	else if( typeId == asTYPEID_UINT32 )        return "UInt32";
	else if( typeId == asTYPEID_UINT64 )        return "UInt64";
    else if( typeId == asTYPEID_FLOAT )         return "Float";
	else if( typeId == asTYPEID_DOUBLE )        return "Double";
    else if( typeId & asTYPEID_OBJHANDLE )      return "Object Handle";
    else if ( typeId & asTYPEID_SCRIPTOBJECT)   return "Script Object";
    else if (typeId & asTYPEID_APPOBJECT)       return "App Object";
    else if ( typeId & asTYPEID_TEMPLATE)       return "Template";
    else return "Unkown Type";
}

StackManager::StackManager()
{
    m_stackTree = new ValueTree("CallStack");
}

ValueTree StackManager::getStackTree()
{
    const ScopedLock sl(m_stackTreeLock);
    m_stackTreeForGUI = new ValueTree(m_stackTree->createCopy());
    return *m_stackTreeForGUI;
}

void StackManager::updateStack(asIScriptContext *ctx)
{
    const ScopedLock sl(m_stackTreeLock);
    
    m_stackTree->removeAllChildren(nullptr);
    
    const char *filePath;
    int lineNumber = ctx->GetLineNumber(0,0, &filePath);
    m_stackTree->setProperty("CurrentLineNum", lineNumber, nullptr);
    m_stackTree->setProperty("CurrentFile", (String) filePath, nullptr);
    
    {
        // This will move to the debugger GUI later
        File scriptFile(filePath);
        StringArray scriptLines;
        scriptFile.readLines(scriptLines);
        DBG("Line: " + (String) lineNumber + " - " + scriptLines[lineNumber-1]);
    }
    
    for (int i = ctx->GetCallstackSize(); i-- > 0;)
    {
        m_stackTree->addChild(updateStackLevel(ctx, i), -1, nullptr);
    }
    
    sendChangeMessage();
}


ValueTree StackManager::updateStackLevel(asIScriptContext *ctx, int stackLevel)
{
    ValueTree treeStackLevel ("StackLevel");
    treeStackLevel.setProperty("LevelNum", stackLevel, nullptr);
    
    asIScriptFunction *function = ctx->GetFunction(stackLevel);
    treeStackLevel.setProperty("StackFunctionDeclaration", (String) function->GetDeclaration(), nullptr);
    
    int numOfVariables = ctx->GetVarCount();
    treeStackLevel.setProperty("NumOfVariables", numOfVariables, nullptr);
    
    // Collect all of the known variables
    ValueTree treeStackLevelVariables ("Variables");
    
    for (int i = 0; i < ctx->GetVarCount(stackLevel); i++)
    {
        ValueTree variableTree = createEmptyVariableTree();
        
        String varName = ctx->GetVarName(i, stackLevel);
        setVariableName(variableTree, varName);
        
        bool isVarInScope = ctx->IsVarInScope(i, stackLevel);
        setVariableScope(variableTree, isVarInScope);
        
        void* obj = ctx->GetAddressOfVar(i, stackLevel);
        int typeId = ctx->GetVarTypeId(i, stackLevel);
        
        if (isVarInScope)
            handleEngineObject(obj, typeId, treeStackLevelVariables, variableTree);
        else
            setVariableTypeId(variableTree, typeId);
        
        treeStackLevelVariables.addChild(variableTree, -1, nullptr);
    }
    
    treeStackLevel.addChild(treeStackLevelVariables, -1, nullptr);
    
    return treeStackLevel;
}

ValueTree StackManager::createEmptyVariableTree()
{
    ValueTree variableTree ("Variable");
    variableTree.setProperty("Name", "NULL", nullptr);
    variableTree.setProperty("TypeID", 0, nullptr);
    variableTree.setProperty("TypeNamed", "NULL", nullptr);
    variableTree.setProperty("IsInScope", false, nullptr);
    variableTree.setProperty("IsObjHandle", false, nullptr);
    variableTree.setProperty("Address", "NULL", nullptr);
    variableTree.setProperty("Value", "NULL", nullptr);
    
    return variableTree;
}

void StackManager::setVariableName(ValueTree variableTree, const String &name)
{
    variableTree.setProperty("Name", name, nullptr);
}

void StackManager::setVariableTypeId(ValueTree variableTree, const int &typeId, const String &typeName)
{
    variableTree.setProperty("TypeID", typeId, nullptr);
    if (typeName.isEmpty())
        variableTree.setProperty("TypeNamed", typeIDToString(typeId), nullptr);
    else
        variableTree.setProperty("TypeNamed", typeName, nullptr);
}

void StackManager::setVariableValue(ValueTree variableTree, const juce::String &value)
{
    variableTree.setProperty("Value", value, nullptr);
}

void StackManager::setVariableScope(ValueTree variableTree, const bool &isInScope)
{
    variableTree.setProperty("IsInScope", isInScope, nullptr);
}

void StackManager::setVariableIsObjHandle(ValueTree variableTree, const bool &isObjHandle)
{
    variableTree.setProperty("IsObjHandle", isObjHandle, nullptr);
}

void StackManager::setVariableAddress(juce::ValueTree variableTree, const String &address)
{
    variableTree.setProperty("Address", address, nullptr);
}

void StackManager::handleEngineObject(const void *obj, const int &typeId, ValueTree stackVariables, ValueTree variableTree)
{
    if (isFundementalType(typeId))
        handleFundementalTypes(obj, typeId, variableTree);
    else
        handleNonFundementalTypes(obj, typeId, stackVariables, variableTree);
}

bool StackManager::isFundementalType(const int &typeId)
{
    if (typeId == asTYPEID_BOOL     ||
        typeId == asTYPEID_INT8     ||
        typeId == asTYPEID_INT16    ||
        typeId == asTYPEID_INT32    ||
        typeId == asTYPEID_INT64    ||
        typeId == asTYPEID_UINT8    ||
        typeId == asTYPEID_UINT16   ||
        typeId == asTYPEID_UINT32   ||
        typeId == asTYPEID_UINT64   ||
        typeId == asTYPEID_FLOAT    ||
        typeId == asTYPEID_DOUBLE )
        return true;
    return false;
}

String StackManager::fundementalTypeToString(const void *value, const int &typeId)
{
    if (typeId == asTYPEID_VOID)            return "Void";
    else if( typeId == asTYPEID_BOOL )      return *(bool*)value ? "true" : "false";
	else if( typeId == asTYPEID_INT8 )      return (String) *(int8*)value;
	else if( typeId == asTYPEID_INT16 )     return (String) *(int16*)value;
	else if( typeId == asTYPEID_INT32 )     return (String) *(int32*)value;
	else if( typeId == asTYPEID_INT64 )     return (String) *(int64*)value;
    else if( typeId == asTYPEID_UINT8 )     return (String) *(uint8*)value;
	else if( typeId == asTYPEID_UINT16 )    return (String) *(uint16*)value;
	else if( typeId == asTYPEID_UINT32 )    return (String) *(uint32*)value;
	else if( typeId == asTYPEID_UINT64 )    return (String) *(uint64*)value;
    else if( typeId == asTYPEID_FLOAT )     return (String) *(float*)value;
	else if( typeId == asTYPEID_DOUBLE )    return (String) *(double*)value;
    else return String("Unkown Type");
}

String StackManager::getAddressOf(const void *obj)
{
    std::stringstream stream;
    stream << obj;
    return String(stream.str());
}

void StackManager::handleFundementalTypes(const void *obj, const int &typeId, juce::ValueTree variableTree)
{
    setVariableTypeId(variableTree, typeId);
    setVariableAddress(variableTree, getAddressOf(&obj));
    setVariableValue(variableTree, fundementalTypeToString(obj, typeId));
}

void StackManager::handleNonFundementalTypes(const void *obj, const int &typeId, ValueTree stackVariables, juce::ValueTree variableTree)
{
    setVariableTypeId(variableTree, typeId);
    if (typeId & asTYPEID_OBJHANDLE)                handleObjHandle(obj, typeId, stackVariables, variableTree);
    else if (typeId & asTYPEID_SCRIPTOBJECT)        handleScriptObj(obj, typeId, stackVariables, variableTree);
    else if (typeId & asTYPEID_TEMPLATE)            handleTemplate(obj, typeId, variableTree);
    else if (typeId & asTYPEID_APPOBJECT)           handleAppObj(obj, typeId, variableTree);
}

void StackManager::handleObjHandle(const void *obj, const int &typeId, ValueTree stackVariables, juce::ValueTree variableTree)
{
    void* object = *(void**)obj;
    
    if (object == nullptr)
        setVariableValue(variableTree, "NULL");
    else
    {
        String address = getAddressOf(object);
        setVariableValue(variableTree, address);
        
        // Search the stack of variables for a variable / object that matches the address of the pointer.
        for (int i = 0; i < stackVariables.getNumChildren(); i++)
        {
            if (stackVariables.getChild(i).getProperty("Address") == address)
            {
                ValueTree match = stackVariables.getChild(i);
                setVariableTypeId(variableTree, match.getProperty("TypeID"), match.getProperty("TypeNamed"));
                setVariableIsObjHandle(variableTree, true);
                // Make a copy of the found type's existing ValueTree
                variableTree.addChild(match.createCopy(), -1, nullptr);
                break;
            }
        }
    }
    
}

void StackManager::handleScriptObj(const void *obj, const int &typeId, ValueTree stackVariables, juce::ValueTree variableTree)
{
    setVariableAddress(variableTree, getAddressOf(obj));
    
    // Dereference pointer
    if (typeId & asTYPEID_OBJHANDLE)
        obj = *(void**)obj;
    
    asIScriptObject* object = (asIScriptObject*) obj;
    
    if (object != nullptr)
    {
        //setVariableName(variableTree, object->GetObjectType()->GetName());
        setVariableValue(variableTree, getAddressOf(obj));
        setVariableTypeId(variableTree, typeId, object->GetObjectType()->GetName());
        
        for (int i = 0; i < object->GetPropertyCount(); i++)
        {
            ValueTree newVariable = createEmptyVariableTree();
            setVariableName(newVariable, (String) object->GetPropertyName(i));
            setVariableScope(newVariable, true);
            handleEngineObject(object->GetAddressOfProperty(i), object->GetPropertyTypeId(i), stackVariables, newVariable);
            variableTree.addChild(newVariable, -1, nullptr);
        }
    }
}

void StackManager::handleTemplate(const void *obj, const int &typeId, juce::ValueTree variableTree)
{
    CScriptArray* array = (CScriptArray*) obj;
    
    if (array != nullptr)
    {
        setVariableTypeId(variableTree, array->GetElementTypeId());
        setVariableValue(variableTree, "");
        for (int i = 0; i < array->GetSize(); i++)
        {
            void* ptr = array->At(i);
            DBG(fundementalTypeToString(ptr, array->GetElementTypeId()));
            ValueTree newVariable = createEmptyVariableTree();
            setVariableName(newVariable, String("[" + (String) i + "]"));
            setVariableScope(newVariable, true);
            handleEngineObject(ptr, array->GetElementTypeId(), newVariable, newVariable);
            variableTree.addChild(newVariable, -1, nullptr);
        }
        
    }
}

void StackManager::handleAppObj(const void *obj, const int &typeId, juce::ValueTree variableTree)
{
    std::string* stringObject = (std::string*) obj;
    if (stringObject != nullptr)
    {
        setVariableTypeId(variableTree, typeId, "String");
        String text = "\"" + (String) *stringObject + "\"";
        setVariableValue(variableTree, text);
    }
}

void printStackTree (ValueTree* treeToPrint)
{
    DBG(treeToPrint->getType().toString());
    for (int i = 0; i < treeToPrint->getNumProperties(); i++) {
        DBG(treeToPrint->getPropertyName(i).toString() + " - " +
            treeToPrint->getProperty(treeToPrint->getPropertyName(i)).toString()
            );
    }
    for (int i = 0; i < treeToPrint->getNumChildren(); i++) {
        ValueTree tree(treeToPrint->getChild(i));
        printStackTree(&tree);
    }
    DBG("");
}
