/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef __MAINCOMPONENT_H_4CD435DC__
#define __MAINCOMPONENT_H_4CD435DC__

#include "../JuceLibraryCode/JuceHeader.h"
#include "./View/DebugTextBox.h"
#include "./View/Debugger/MainDebugComponent.h"
#include "./Helpers/VersionHelper.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   :  public Component,
                                public ChangeListener
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();
    
    void changeListenerCallback(ChangeBroadcaster *source);
    
    void paint (Graphics&);
    void resized();

private:
    //==============================================================================
    LookAndFeel_V3 lookAndFeel_V3;
    ScopedPointer<DebugConsole> m_debugConsole;
    ScopedPointer<MainDebugComponent> m_mainDebugComponent;
    ScopedPointer<TabbedComponent> m_mainTabbedComponent;
    
    void setDebugTagView();
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // __MAINCOMPONENT_H_4CD435DC__
