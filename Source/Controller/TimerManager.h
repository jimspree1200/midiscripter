//
//  TimerManager.h
//  MaoJ
//
//  Created by James Mazur on 11/30/13.
//
//

#ifndef __MaoJ__TimerManager__
#define __MaoJ__TimerManager__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

class AngelScriptEngine;

class NamedTimer
{
public:
    NamedTimer(){};
    NamedTimer(const String &name, const int repeats);
    NamedTimer(const String &name);
    
    NamedTimer operator ++(int);
    NamedTimer operator --(int);

    void setName(const String& name) { m_name = name; };
    bool isName(const String& name) { return name == m_name; };
    String getName() const { return m_name; };

    void setRepeats(const int repeats) { m_repeats = repeats; };
    int getRepeats() { return m_repeats; };
    void decrement() { m_repeats--; };
    void increment() { m_repeats++; };
    
    bool isContinuous() { return m_runContinuous; };
    void setContinuous(bool runContinuously ) { m_runContinuous = runContinuously; };
    
private:
    String m_name;
    int m_repeats = 0;
    bool m_runContinuous = false;
};

class TimerManager : public MultiTimer
{
    
public:
    TimerManager(AngelScriptEngine& engine);
    void startNamedTimer (const String &timerName, const int milliseconds, const int repeats);
    void stopNamedTimer (const String &timerName, const int milliseconds);
    void reset();
    
    void timerCallback (int timerID);
    
private:
    bool hasNamedTimer (const String &timerName);
    int indexOf(const String &timerName);
    
    
private:
    Array<NamedTimer> m_timers;
    AngelScriptEngine& m_engine;
    
};



#endif /* defined(__MaoJ__TimerManager__) */
