//
//  TimerManager.cpp
//  MaoJ
//
//  Created by James Mazur on 11/30/13.
//
//

#include "TimerManager.h"
#include "../AngelScript/AngelScriptEngine.h"

NamedTimer::NamedTimer(const String &name, const int repeats)
:
m_name(name),
m_repeats(repeats)
{
}

NamedTimer::NamedTimer(const String &name)
:
m_name(name),
m_runContinuous(true)
{
}

NamedTimer NamedTimer::operator++(int)
{
    m_repeats++;
    return *this;
}

NamedTimer NamedTimer::operator--(int)
{
    m_repeats--;
    return *this;
}

TimerManager::TimerManager(AngelScriptEngine& engine)
:
m_engine(engine)
{
}

void TimerManager::startNamedTimer(const juce::String &timerName, const int milliseconds, const int repeats)
{
    if (!this->hasNamedTimer(timerName))
    {
        if (repeats > 0)
        {
            NamedTimer timer(timerName, repeats);
            m_timers.add(timer);
        }
        else
        {
            NamedTimer timer(timerName);
            m_timers.add(timer);
        }
        startTimer(indexOf(timerName), milliseconds);
    }
    else
    {
        NamedTimer* timer;
        timer = &m_timers.getReference(indexOf(timerName));
        if (repeats > 0 && timer->isContinuous()) {
            timer->setContinuous(false);
            timer->setRepeats(repeats);
        }
        if (repeats <= 0 && !timer->isContinuous())
        {
            timer->setContinuous(true);
            timer->setRepeats(0);
        }
        startTimer(indexOf(timerName), milliseconds);
    }
        
}

void TimerManager::stopNamedTimer(const juce::String &timerName, const int milliseconds)
{
    if (hasNamedTimer(timerName))
        stopTimer(indexOf(timerName));

}

void TimerManager::reset()
{
    for (int i = 0; i < m_timers.size(); i++) {
        stopTimer(i);
    }
    m_timers.clear();
}  

void TimerManager::timerCallback (int timerID)
{
    DBG(String(m_timers[timerID].getRepeats()) + ": " + m_timers[timerID].getName() + " - Timer Callback");
    
    NamedTimer *currentTimer;
    currentTimer = &m_timers.getReference(timerID);
    
    if (currentTimer->getRepeats() <= 0 && !currentTimer->isContinuous()) {
        stopTimer(timerID);
        return;
    }
    
    if (!currentTimer->isContinuous())
        (*currentTimer)--;
    
    m_engine.engineTimerCallback(currentTimer->getName());
}

bool TimerManager::hasNamedTimer(const juce::String &timerName)
{
    for (int i = 0; i < m_timers.size(); i++)
    {
        if (m_timers[i].isName(timerName))
            return true;
    }
    return false;
}

int TimerManager::indexOf(const juce::String &timerName)
{
    for (int i = 0; i < m_timers.size(); i++) {
        if (m_timers[i].isName(timerName)) return i;
    }
    return -1;
}