//
//  MidiPortManager.cpp
//  MaoJ
//
//  Created by James Mazur on 11/13/13.
//
//

#include "MidiPortManager.h"

class MidiPortManager::MidiCallbackHandler  : public MidiInputCallback
{
public:
    MidiCallbackHandler (MidiPortManager& adm) noexcept  : owner (adm) {}
    
private:
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override
    {
        owner.handleIncomingMidiMessageInt (source, message);
    }
    
    MidiPortManager& owner;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiCallbackHandler)
};

NamedMidiOutput::~NamedMidiOutput()
{
    if (m_threadRunning && m_portOpened)
        m_midiOutput->stopBackgroundThread();
}

bool NamedMidiOutput::openPort(juce::String name)
{
    // Look up port
    const int index = MidiOutput::getDevices().indexOf(name);
    // If the port is not found, return false
    if (index < 0)
        return false;
    
    // Open the device
    m_midiOutput = MidiOutput::openDevice(index);
    // Check that the device was opened
    if (m_midiOutput == nullptr)
        return false;
    m_portOpened = true;
    
    // Set the port name
    m_midiOutputName = name;
    
    return true;
}

bool NamedMidiOutput::createPort(juce::String name)
{
#if JUCE_LINUX || JUCE_MAC || JUCE_IOS || DOXYGEN

    m_midiOutput = MidiOutput::createNewDevice(name);
    // Check that the device was opened
    if (m_midiOutput == nullptr)
        return false;
    m_portOpened = true;
    
    // Set the port name
    m_midiOutputName = name;
    
    return true;

#endif

	return false;
}

juce::String NamedMidiOutput::getName() const
{
    return m_midiOutputName;
}

void NamedMidiOutput::sendMessageNow(const juce::MidiMessage &message)
{
    if (!m_portOpened) return;
    m_midiOutput->sendMessageNow(message);
}

void NamedMidiOutput::sendDelayedMessage(const juce::MidiMessage &message, const int delayInMs)
{
    checkIfThreadIsRunning();
    
    const uint32 timeToSendAt = Time::getMillisecondCounter() + delayInMs;
    
    MidiBuffer midiBuffer( message );
    
    m_midiOutput->sendBlockOfMessages(midiBuffer, timeToSendAt, samplesPerSecond);
}

void NamedMidiOutput::sendBufferedMessage(const juce::MidiMessage &message)
{
    if (!m_portOpened) return;
    
    checkIfThreadIsRunning();
    
    const uint32 currentTime = Time::getMillisecondCounter();
    double timeToSendAt;
    //DBG("Current = " + (String)(UInt32)currentTime + " - " + (String)(UInt32)m_nextTimestampToSend);
    if (currentTime <= m_nextTimestampToSend)
    {
        timeToSendAt = m_nextTimestampToSend;
        m_nextTimestampToSend += m_millisecondsInterval;
        //DBG("Next:" + (String)(UInt32)timeToSendAt);
    }
    else
    {
        timeToSendAt = currentTime;
        m_nextTimestampToSend = currentTime + m_millisecondsInterval;
        //DBG("Current:" + (String)(UInt32)timeToSendAt);
    }
    
    MidiBuffer midiBuffer( message );
    m_midiOutput->sendBlockOfMessages(midiBuffer, timeToSendAt, samplesPerSecond);
}

void NamedMidiOutput::setBufferInterval(const int intervalToSet)
{
    checkIfThreadIsRunning();
    m_millisecondsInterval = intervalToSet;
}

void NamedMidiOutput::checkIfThreadIsRunning()
{
    if (m_threadRunning) return;
    m_midiOutput->startBackgroundThread();
    m_threadRunning = true;
}

MidiPortManager::MidiPortManager()
{
    midiCallbackHandler = new MidiCallbackHandler (*this);
    StringArray devices = MidiOutput::getDevices();
    for (int i = 0; i < devices.size(); i++) {
        DBG(devices[i]);
    }
    
}

MidiPortManager::~MidiPortManager()
{
}

void MidiPortManager::enableAllMidiInputs()
{
    const StringArray inputs = MidiInput::getDevices();
    
    for (auto index = inputs.size(); index > 0; index--)
    {
        DBG(inputs[index-1]);
        if (MidiInput* const midiIn = MidiInput::openDevice(index-1, this->midiCallbackHandler))
        {
            DBG("opened: " + midiIn->getName());
            enabledMidiInputs.add (midiIn);
            midiIn->start();
        }
    }
}

void MidiPortManager::disableAllMidiInputs()
{
    //removeAllMidiInputCallbacks();
    while (enabledMidiInputs.size() != 0) {
        enabledMidiInputs.removeLast();
    }
    
}

void MidiPortManager::disableAllMidiOutputs()
{
    while (enabledMidiOutputs.size() != 0) {
        enabledMidiOutputs.removeLast();
    }
}

void MidiPortManager::createVirtualMidiInput(const juce::String &midiInputDeviceName)
{
#if JUCE_LINUX || JUCE_MAC || JUCE_IOS || DOXYGEN
    if (!isMidiInputEnabled (midiInputDeviceName))
    {
        if (MidiInput* const midiIn = MidiInput::createNewDevice(midiInputDeviceName, this->midiCallbackHandler))
        {
            enabledMidiInputs.add (midiIn);
            midiIn->start();
        }
    }
#endif
}

void MidiPortManager::setMidiInputEnabled (const String& name, const bool enabled)
{
    if (enabled != isMidiInputEnabled (name))
    {
        if (enabled)
        {
            const int index = MidiInput::getDevices().indexOf (name);
            
            if (index >= 0)
            {
                if (MidiInput* const midiIn = MidiInput::openDevice (index, this->midiCallbackHandler))
                {
                    enabledMidiInputs.add (midiIn);
                    midiIn->start();
                }
            }
        }
        else
        {
            for (int i = enabledMidiInputs.size(); --i >= 0;)
                if (enabledMidiInputs[i]->getName() == name)
                    enabledMidiInputs.remove (i);
        }
        
        //updateXml();
        //sendChangeMessage();
    }
}

void MidiPortManager::createVirtualMidiOutput(const juce::String &midiOutputDeviceName)
{
    if (!isMidiInputEnabled(midiOutputDeviceName))
    {
        NamedMidiOutput* output = new NamedMidiOutput;
        enabledMidiOutputs.add(output);
        if (!enabledMidiOutputs.getLast()->createPort(midiOutputDeviceName))
            enabledMidiOutputs.removeLast();
    }
}

void MidiPortManager::setMidiOutputEnabled (const String& name, const bool enabled)
{
    if (enabled != isMidiOutputEnabled (name))
    {
        if (enabled)
        {
            NamedMidiOutput* output = new NamedMidiOutput;
            enabledMidiOutputs.add(output);
            if (!enabledMidiOutputs.getLast()->openPort(name))
            {
                enabledMidiOutputs.removeLast();
            }
        }
        else
        {
            for (int i = enabledMidiOutputs.size(); --i >= 0;)
                if (enabledMidiOutputs[i]->getName() == name)
                    enabledMidiOutputs.remove (i);
        }
        
        //updateXml();
        //sendChangeMessage();
    }
}

bool MidiPortManager::isMidiInputEnabled (const String& name) const
{
    for (int i = enabledMidiInputs.size(); --i >= 0;)
        if (enabledMidiInputs[i]->getName() == name)
            return true;
    
    return false;
}

bool MidiPortManager::isMidiOutputEnabled(const juce::String &midiOutputDeviceName) const
{
    for (int i = enabledMidiOutputs.size(); --i >= 0;) {
        if (enabledMidiOutputs[i]->getName() == midiOutputDeviceName)
            return true;
    }
    return false;
}

void MidiPortManager::addMidiInputCallback (const String& name, MidiInputCallback* callbackToAdd)
{
    removeMidiInputCallback (name, callbackToAdd);
    
    if (name.isEmpty() || isMidiInputEnabled (name))
    {
        const ScopedLock sl (midiCallbackLock);
        midiCallbacks.add (callbackToAdd);
        midiCallbackDevices.add (name);
    }
}

void MidiPortManager::removeMidiInputCallback (const String& name, MidiInputCallback* callbackToRemove)
{
    for (int i = midiCallbacks.size(); --i >= 0;)
    {
        if (midiCallbackDevices[i] == name && midiCallbacks.getUnchecked(i) == callbackToRemove)
        {
            const ScopedLock sl (midiCallbackLock);
            midiCallbacks.remove (i);
            midiCallbackDevices.remove (i);
        }
    }
}

void MidiPortManager::removeAllMidiInputCallbacks()
{
    for (int i = midiCallbacks.size(); --i >= 0;) {
        const ScopedLock sl (midiCallbackLock);
        midiCallbacks.remove(i);
        midiCallbackDevices.remove(i);
    }
}

void MidiPortManager::sendMidiMessage(const juce::String midiOutputDeviceName, const juce::MidiMessage &message, const bool shouldBeBuffered)
{
    for (int i = enabledMidiOutputs.size(); --i>=0;)
    {
        if (enabledMidiOutputs[i]->getName() == midiOutputDeviceName)
        {
            if (shouldBeBuffered)
                enabledMidiOutputs[i]->sendBufferedMessage(message);
            else
                enabledMidiOutputs[i]->sendMessageNow(message);
        }
    }
}

void MidiPortManager::setDefaultBufferInterval(const juce::String midiOutputDeviceName, const int millisecondsBetweenMessages)
{
    for (int i = enabledMidiOutputs.size(); --i>=0;)
    {
        if (enabledMidiOutputs[i]->getName() == midiOutputDeviceName)
            enabledMidiOutputs[i]->setBufferInterval(millisecondsBetweenMessages);
    }
}

void MidiPortManager::sendDelayedMidiMessage(const juce::String midiOutputDeviceName, const juce::MidiMessage &message, const int delayInMs)
{
    for (int i = enabledMidiOutputs.size(); --i>=0;)
    {
        if (enabledMidiOutputs[i]->getName() == midiOutputDeviceName)
            enabledMidiOutputs[i]->sendDelayedMessage(message, delayInMs);
    }
}

void MidiPortManager::handleIncomingMidiMessageInt (MidiInput* source, const MidiMessage& message)
{
    if (! message.isActiveSense())
    {
        const bool isDefaultSource = (source == nullptr || source == enabledMidiInputs.getFirst());
        if (isDefaultSource)
        const ScopedLock sl (midiCallbackLock);
        
        for (int i = midiCallbackDevices.size(); --i >= 0;)
        {
            const String name (midiCallbackDevices[i]);
                        
            if ((name.isEmpty()) || (name.isNotEmpty() && name == source->getName()))
                midiCallbacks.getUnchecked(i)->handleIncomingMidiMessage (source, message);
        }
    }
}