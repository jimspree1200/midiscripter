//
//  MidiPortManager.h
//  MaoJ
//
//  Created by James Mazur on 11/13/13.
//
//

#ifndef __MaoJ__MidiPortManager__
#define __MaoJ__MidiPortManager__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

class NamedMidiOutput {

public:
    NamedMidiOutput() {};
    ~NamedMidiOutput();
    
    bool openPort(String name);
    bool createPort(String name);
    
    String getName() const;
    
    // Sends the MIDI message immediately
    void sendMessageNow(const MidiMessage &message);
    
    void sendDelayedMessage(const MidiMessage& message, const int delayInMs);
    
    // Sends the MIDI message with a delay if other MIDI messages are already queued up
    void sendBufferedMessage(const MidiMessage &message);
    
    // Sets the interval in which the MIDI messages should be sent
    void setBufferInterval(const int intervalToSet);
    
private:
    void checkIfThreadIsRunning();
    
    ScopedPointer<MidiOutput> m_midiOutput;
    String m_midiOutputName = "none";
    bool m_threadRunning = false;
    bool m_portOpened = false;
    uint32 m_nextTimestampToSend = 0;
    const double samplesPerSecond = 1000;
    int m_millisecondsInterval = 2;
};

class MidiPortManager {
    
public:
    MidiPortManager();
    ~MidiPortManager();
    void enableAllMidiInputs ();
    void disableAllMidiInputs ();
    void disableAllMidiOutputs ();
    void createVirtualMidiInput (const String& midiInputDeviceName);
    void setMidiInputEnabled (const String& midiInputDeviceName, bool enabled);
    void createVirtualMidiOutput (const String& midiOutputDeviceName);
    void setMidiOutputEnabled (const String& midiInputDeviceName, bool enabled);
    bool isMidiInputEnabled (const String& midiInputDeviceName) const;
    bool isMidiOutputEnabled (const String& midiOutputDeviceName) const;
    void addMidiInputCallback (const String& midiInputDeviceName,
                               MidiInputCallback* callback);
    void removeMidiInputCallback (const String& midiInputDeviceName,
                                  MidiInputCallback* callback);
    void removeAllMidiInputCallbacks ();
    void sendMidiMessage(const String midiOutputDeviceName, const MidiMessage& message, const bool shouldBeBuffered);
    void setDefaultBufferInterval (const juce::String midiOutputDeviceName, const int millisecondsBetweenMessages);
    void sendDelayedMidiMessage(const String midiOutputDeviceName, const MidiMessage& message, const int delayInMs);
    
    /** Returns the a lock that can be used to synchronise access to the midi callback.
     Obviously while this is locked, you're blocking the midi system from running, so
     it must only be used for very brief periods when absolutely necessary.
     */
    CriticalSection& getMidiCallbackLock() noexcept         { return midiCallbackLock; }
    
private:

    //StringArray midiInsFromXml;
    OwnedArray <MidiInput> enabledMidiInputs;
    Array <MidiInputCallback*> midiCallbacks;
    StringArray midiCallbackDevices;
    String defaultMidiOutputName;
    ScopedPointer <MidiOutput> defaultMidiOutput;
    CriticalSection midiCallbackLock;
    
    OwnedArray<NamedMidiOutput> enabledMidiOutputs;
    OwnedArray<MidiOutput> testoutputs;
    
    class MidiCallbackHandler;
    friend class MidiCallbackHandler;
    friend struct ContainerDeletePolicy<MidiCallbackHandler>;
    ScopedPointer<MidiCallbackHandler> midiCallbackHandler;
    
    void handleIncomingMidiMessageInt (MidiInput*, const MidiMessage&);
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MidiPortManager)
};

#endif /* defined(__MaoJ__MidiPortManager__) */
