/*
  ==============================================================================

    CommandIDs.h
    Created: 31 Jul 2014 12:45:06pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef COMMANDIDS_H_INCLUDED
#define COMMANDIDS_H_INCLUDED


/**
 A namespace to hold all the possible command IDs.
 */
namespace CommandIDs
{
    enum
    {
        open                   = 0x200010,
        
        reloadScript           = 0x200020,
        buildScript            = 0x200030,
        runScript              = 0x200031,
        stopScript             = 0x200032,
        
        toggleDebug            = 0x201000,
        
        toggleBreakpoints      = 0x201020,
        
        pauseContinueExecution = 0x202000,
        stepOver               = 0x202010,
        stepInto               = 0x202011,
        stepOut                = 0x202013
    };
}

namespace CommandCategories
{
    static const char* const general       = "General";
    static const char* const debugging       = "Debugging";
}

//closeProject           = 0x200051,
//saveProject            = 0x200060,
//saveAll                = 0x200080,
//openInIDE              = 0x200072,
//saveAndOpenInIDE       = 0x200073,
//
//showUTF8Tool           = 0x200076,
//showAppearanceSettings = 0x200077,
//showConfigPanel        = 0x200074,
//showFilePanel          = 0x200078,
//showTranslationTool    = 0x200079,
//showProjectSettings    = 0x20007a,
//showProjectModules     = 0x20007b,
//showSVGPathTool        = 0x20007c,
//
//closeWindow            = 0x201001,
//closeAllDocuments      = 0x201000,
//goToPreviousDoc        = 0x201002,
//goToNextDoc            = 0x201003,
//goToCounterpart        = 0x201004,
//deleteSelectedItem     = 0x201005,
//
//showFindPanel          = 0x2010a0,
//findSelection          = 0x2010a1,
//findNext               = 0x2010a2,
//findPrevious           = 0x2010a3

#endif  // COMMANDIDS_H_INCLUDED
