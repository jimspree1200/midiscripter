/*
  ==============================================================================

    StoredSettings.cpp
    Created: 31 Jul 2014 8:21:11am
    Author:  Jim Mazur

  ==============================================================================
*/

#include "StoredSettings.h"
#include "../Application.h"

//==============================================================================

StoredSettings& getAppSettings()
{
    return *MidiScripterApp::getApp().settings;
}

PropertiesFile& getGlobalProperties()
{
    return getAppSettings().getGlobalProperties();
}

//==============================================================================

StoredSettings::StoredSettings()
{
    reload();
    for (int i = 0; i < recentFiles.getNumFiles(); i++) {
        DBG("Stored");
        DBG(recentFiles.toString());
    }
}

StoredSettings::~StoredSettings()
{
    flush();
}

PropertiesFile& StoredSettings::getGlobalProperties()
{
    return *m_propertyFiles.getUnchecked (0);
}

static PropertiesFile* createPropsFile (const String& filename)
{
    return new PropertiesFile (MidiScripterApp::getApp()
                               .getPropertyFileOptionsFor (filename));
}

void StoredSettings::updateGlobalProps()
{
    PropertiesFile& props = getGlobalProperties();
    
    props.setValue ("recentFiles", recentFiles.toString());
    
}

void StoredSettings::flush()
{
    updateGlobalProps();
    
    for (int i = m_propertyFiles.size(); --i >= 0;)
        m_propertyFiles.getUnchecked(i)->saveIfNeeded();
}

void StoredSettings::reload()
{
    m_propertyFiles.clear();
    m_propertyFiles.add(createPropsFile ("MidiScripter"));
    
    // recent files...
    recentFiles.restoreFromString (getGlobalProperties().getValue ("recentFiles"));
    recentFiles.removeNonExistentFiles();
}