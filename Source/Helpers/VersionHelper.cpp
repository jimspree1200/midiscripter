//
//  VersionHelper.cpp
//  MaoJ
//
//  Created by Jim Mazur on 12/8/13.
//
//

#include "VersionHelper.h"

//TODO: Cleanup leaks

String getVersion()
{
    String path = File::getSpecialLocation(File::SpecialLocationType::currentApplicationFile).getFullPathName() + "/Contents/Info.plist";
    File plist(path);
    XmlDocument plistXML (plist);
    ScopedPointer<XmlElement> plistElement = plistXML.getDocumentElement();
    StringRef("CFBundleVersion");
    XmlElement* dict = plistElement->getChildByName(StringRef("dict"));
    if (dict == nullptr) {
        DBG("fail");
    }
    String returnString = "Unknown";
    XmlElement* child;
    for (int i = 0; i < dict->getNumChildElements(); i++)
    {
        child = dict->getChildElement(i);
        if(child->getTagName()=="key" && child->getChildElement(0)->getText() == "CFBundleVersion")
        {
                returnString = child->getNextElement()->getChildElement(0)->getText();
        }
    }
    return returnString;
}