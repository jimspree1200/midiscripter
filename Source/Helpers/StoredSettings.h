/*
  ==============================================================================

    StoredSettings.h
    Created: 31 Jul 2014 8:21:11am
    Author:  Jim Mazur
 
 NOTE: Derived from JUCE's IntroJucer source code

  ==============================================================================
*/

#ifndef STOREDSETTINGS_H_INCLUDED
#define STOREDSETTINGS_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

class StoredSettings {
    
public:
    StoredSettings();
    ~StoredSettings();
    
    PropertiesFile& getGlobalProperties();
    PropertiesFile& getScriptProperties (const String& projectUID);
    
    void flush();
    void reload();
    
    RecentlyOpenedFilesList recentFiles;
    
private:
    OwnedArray<PropertiesFile> m_propertyFiles;
    
    void updateGlobalProps();
};

StoredSettings& getAppSettings();
PropertiesFile& getGlobalProperties();


#endif  // STOREDSETTINGS_H_INCLUDED
