//
//  VersionHelper.h
//  MaoJ
//
//  Created by Jim Mazur on 12/8/13.
//
//

#ifndef __MaoJ__VersionHelper__
#define __MaoJ__VersionHelper__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

String getVersion();

#endif /* defined(__MaoJ__VersionHelper__) */
