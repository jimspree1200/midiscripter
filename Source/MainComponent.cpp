/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include "Application.h"


//==============================================================================
MainContentComponent::MainContentComponent()
{
    LookAndFeel::setDefaultLookAndFeel(&lookAndFeel_V3);

    m_mainTabbedComponent = new TabbedComponent(TabbedButtonBar::TabsAtTop);
    m_mainTabbedComponent->addTab("Logger", Colours::lightgrey, m_debugConsole = new DebugConsole(), false);
    setDebugTagView();
    
    MidiScripterApp::getAngelScriptEngine().getDebugger()->addChangeListener(this);
    
    setSize (800, 600);
}

MainContentComponent::~MainContentComponent()
{
    MidiScripterApp::getAngelScriptEngine().getDebugger()->removeChangeListener(this);
}

void MainContentComponent::changeListenerCallback(ChangeBroadcaster *source)
{
    if (source == MidiScripterApp::getAngelScriptEngine().getDebugger())
        setDebugTagView();
}

void MainContentComponent::paint (Graphics& g)
{
    g.fillAll (Colours::grey);

}

void MainContentComponent::resized()
{
    m_mainTabbedComponent->setBoundsInset(BorderSize<int> (5));
}

void MainContentComponent::setDebugTagView()
{
    AngelScriptDebugger* debugger = MidiScripterApp::getAngelScriptEngine().getDebugger();
    if (debugger->isEnabled())
    {
        m_mainTabbedComponent->addTab("Debug", Colours::lightgrey, m_mainDebugComponent = new MainDebugComponent(), false);
        addAndMakeVisible(m_mainTabbedComponent);
    }
    else
    {
        m_mainTabbedComponent->setCurrentTabIndex(0);
        m_mainTabbedComponent->removeTab(1);
    }
}