//
//  Application.h
//  MidiScripter
//
//  Created by Jim Mazur on 5/18/14.
//
//

#ifndef MidiScripter_Application_h
#define MidiScripter_Application_h

#include "MainWindow.h"
#include "Helpers/CommandIDs.h"
#include "AngelScript/AngelScriptEngine.h"
#include "./Controller/MidiPortManager.h"
#include "./Helpers/StoredSettings.h"

class MidiScripterApp  : public JUCEApplication
{
public:
    //==============================================================================
    
    const String getApplicationName()       { return ProjectInfo::projectName; }
    const String getApplicationVersion()    { return ProjectInfo::versionString; }
    bool moreThanOneInstanceAllowed()       { return false; }
    
    //==============================================================================
    void initialise (const String& commandLine)
    {
        settings = new StoredSettings();
        
        initialiseAngelScriptEngine();
        
        initialiseCommandManager();
        
        menuModel = new MainMenuModel;
        
        m_mainWindow = new MainWindow();
        
        loadDefaultScript();
        
#if JUCE_MAC
        MenuBarModel::setMacMainMenu (menuModel, nullptr, "Open Recent");
#endif
        
    }
    
    void shutdown()
    {
        m_mainWindow = nullptr; // (deletes our window)
        m_angelScriptEngine = nullptr;
#if JUCE_MAC
        MenuBarModel::setMacMainMenu (nullptr);
#endif
        menuModel = nullptr;
        commandManager = nullptr;
        settings = nullptr;
    }
    
    //==============================================================================
    void systemRequestedQuit()
    {
        quit();
    }
    
    void anotherInstanceStarted (const String& commandLine)
    {
    }
    
    static MidiScripterApp& getApp()
    {
        MidiScripterApp* const app = dynamic_cast<MidiScripterApp*>(JUCEApplication::getInstance());
        jassert(app != nullptr);
        return *app;
    }
    
    static ApplicationCommandManager& getCommandManager()
    {
        ApplicationCommandManager* cm = MidiScripterApp::getApp().commandManager;
        jassert (cm != nullptr);
        return *cm;
    }
    
    static AngelScriptEngine& getAngelScriptEngine()
    {
        AngelScriptEngine* as = MidiScripterApp::getApp().m_angelScriptEngine;
        jassert(as != nullptr);
        return *as;
    }
    
    static MidiPortManager& getMidiPortManager()
    {
        MidiPortManager* mpm = MidiScripterApp::getApp().m_midiPortManager;
        jassert(mpm != nullptr);
        return *mpm;
    }
    
    //==============================================================================
    // Menubar
    //==============================================================================
    
    class MainMenuModel  : public MenuBarModel
    {
    public:
        MainMenuModel()
        {
            setApplicationCommandManagerToWatch (&getCommandManager());
        }
        
        StringArray getMenuBarNames()
        {
            return getApp().getMenuNames();
        }
        
        PopupMenu getMenuForIndex (int /*topLevelMenuIndex*/, const String& menuName)
        {
            PopupMenu menu;
            getApp().createMenu (menu, menuName);
            return menu;
        }
        
        void menuItemSelected (int menuItemID, int /*topLevelMenuIndex*/)
        {
            getApp().handleMainMenuCommand (menuItemID);
        }
    };
    
    enum
    {
        recentProjectsBaseID = 100,
    };
    
    virtual StringArray getMenuNames()
    {
        const char* const names[] = { "File", "Debug", nullptr };
        return StringArray (names);
    }
    
    virtual void createMenu (PopupMenu& menu, const String& menuName)
    {
        if (menuName == "File")             createFileMenu   (menu);
        else if (menuName == "Debug")       createDebugMenu   (menu);
        else                                jassertfalse; // names have changed?
    }
    
    virtual void createFileMenu (PopupMenu& menu)
    {
        menu.addCommandItem (commandManager, CommandIDs::open);
        
        PopupMenu recentFiles;
        recentFiles.addItem(300, String("test"));
        settings->recentFiles.createPopupMenuItems (recentFiles, recentProjectsBaseID, true, true, nullptr);
        menu.addSubMenu ("Open Recent", recentFiles);
        
#if ! JUCE_MAC
        menu.addSeparator();
        menu.addCommandItem (commandManager, StandardApplicationCommandIDs::quit);
#endif
    }
    
    virtual void createDebugMenu (PopupMenu& menu)
    {
        menu.addCommandItem (commandManager, CommandIDs::toggleDebug);
        menu.addCommandItem (commandManager, CommandIDs::reloadScript);
        menu.addSeparator();
        menu.addCommandItem(commandManager, CommandIDs::buildScript);
        menu.addCommandItem(commandManager, CommandIDs::runScript);
        menu.addSeparator();
        menu.addCommandItem(commandManager, CommandIDs::toggleBreakpoints);
        menu.addCommandItem(commandManager, CommandIDs::stepOver);
        menu.addCommandItem(commandManager, CommandIDs::stepInto);
        menu.addCommandItem(commandManager, CommandIDs::stepOut);
    }
    
    virtual void handleMainMenuCommand (int menuItemID)
    {
        if (menuItemID >= recentProjectsBaseID && menuItemID < recentProjectsBaseID + 100)
        {
            // open a file from the "recent files" menu
            //openFile (settings->recentFiles.getFile (menuItemID - recentProjectsBaseID));
        }
        else
        {
        }
    }
    
    //==============================================================================
    // CommandManager
    //==============================================================================
    
    ApplicationCommandTarget * 	getNextCommandTarget ()
    {
        return nullptr;
    }
    
    void getAllCommands (Array <CommandID>& commands) override
    {
        JUCEApplication::getAllCommands (commands);
        
        const CommandID ids[] = {
            CommandIDs::open,
        };
        
        commands.addArray (ids, numElementsInArray (ids));
    }
    
    void getCommandInfo (CommandID commandID, ApplicationCommandInfo& result) override
    {
        switch (commandID)
        {
                
            case CommandIDs::open:
                result.setInfo ("Open...", "Opens a script", CommandCategories::general, 0);
                result.defaultKeypresses.add (KeyPress ('o', ModifierKeys::commandModifier, 0));
                break;
                
            default:
                JUCEApplication::getCommandInfo (commandID, result);
                break;
        }
    }
    
    bool perform (const InvocationInfo& info) override
    {
        switch (info.commandID)
        {
            case CommandIDs::open:                      askUserToOpenFile(); break;
            default:                                    return JUCEApplication::perform (info);
        }
        
        return true;
    }
    
    //==============================================================================
    // CommandManager
    //==============================================================================
    
    void loadDefaultScript()
    {
        // Load default script, if found
        String path = File::getSpecialLocation(File::SpecialLocationType::currentApplicationFile).getFullPathName() + "/Contents/Resources/Scripts/Main.as";
        
#if JUCE_DEBUG
        // If we are debugging, use a different script
        path = File::getSpecialLocation(File::SpecialLocationType::userDesktopDirectory).getFullPathName() + "/Debug.as";
#endif
        
        File script(path);
        
        if (script.existsAsFile())
            openScript(script);
        
        else if (settings->recentFiles.getNumFiles())
            script = settings->recentFiles.getFile(0);
        
        if (script.existsAsFile())
            openScript(script);
        
        else getAngelScriptEngine().getLogger()->addEvent(EventSeverity::Info, String("No script loaded"));
    }
    
    void askUserToOpenFile()
    {
        FileChooser fc ("Open File");
        
        if (fc.browseForFileToOpen())
            openScript (fc.getResult());
    }
    
    void openScript(const juce::File &file)
    {
        if (!file.existsAsFile())
            return;
        m_mainWindow->loadScript(file);
    }
    
    virtual PropertiesFile::Options getPropertyFileOptionsFor (const String& filename)
    {
        PropertiesFile::Options options;
        options.applicationName     = filename;
        options.filenameSuffix      = "settings";
        options.osxLibrarySubFolder = "Application Support";
#if JUCE_LINUX
        options.folderName          = "~/.config/MidiScripter";
#else
        options.folderName          = "MidiScripter";
#endif
        
        return options;
    }
    
    ScopedPointer<StoredSettings> settings;
    
    ScopedPointer<ApplicationCommandManager> commandManager;
    
    ScopedPointer<MainMenuModel> menuModel;
    
private:
    ScopedPointer<MainWindow> m_mainWindow;
    ScopedPointer<AngelScriptEngine> m_angelScriptEngine;
    ScopedPointer<MidiPortManager> m_midiPortManager;
    
    void initialiseCommandManager()
    {
        commandManager = new ApplicationCommandManager();
        commandManager->registerAllCommandsForTarget (this);
        
//        {
//            CodeDocument doc;
//            CppCodeEditorComponent ed (File::nonexistent, doc);
//            commandManager->registerAllCommandsForTarget (&ed);
//        }
//        
//        registerGUIEditorCommands();
    }
    
    void initialiseAngelScriptEngine()
    {
        m_midiPortManager = new MidiPortManager;
        m_angelScriptEngine = new AngelScriptEngine;
        m_midiPortManager->addMidiInputCallback(String(), m_angelScriptEngine);
    }
    
};

#endif
