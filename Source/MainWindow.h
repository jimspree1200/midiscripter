/*
  ==============================================================================

    MainWindow.h
    Created: 18 May 2014 8:29:59pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef MAINWINDOW_H_INCLUDED
#define MAINWINDOW_H_INCLUDED

#include "MainComponent.h"

class AngelScriptEngine;

class MainWindow    : public DocumentWindow,
                      public ApplicationCommandTarget
{
public:
    MainWindow();
    
    void closeButtonPressed();
    
    void loadScript(const File& file);
    void reloadScript();
    void buildScript();
    void runScript();
    void stopScript();
    void toggleDebug();

    //==============================================================================
    
    ApplicationCommandTarget* getNextCommandTarget() override;
    void getAllCommands(Array<CommandID> &commands) override;
    void getCommandInfo (CommandID commandID, ApplicationCommandInfo& result);
    bool perform (const InvocationInfo& info) override;
    
    
private:
    
    AngelScriptEngine* m_engine;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
};


#endif  // MAINWINDOW_H_INCLUDED
