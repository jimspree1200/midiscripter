/*
  ==============================================================================

    MainWindow.cpp
    Created: 31 Jul 2014 1:27:16pm
    Author:  Jim Mazur

  ==============================================================================
*/

#include "MainWindow.h"
#include "Application.h"

MainWindow::MainWindow():
DocumentWindow ("MaoJ",
                Colours::lightgrey,
                DocumentWindow::allButtons)
{
    m_engine = &MidiScripterApp::getAngelScriptEngine();
    
    MidiScripterApp::getCommandManager().registerAllCommandsForTarget(this);
    MidiScripterApp::getCommandManager().getKeyMappings();
    addKeyListener(MidiScripterApp::getCommandManager().getKeyMappings());
    
    setContentOwned (new MainContentComponent(), true);
    setResizable(true, true);
    centreWithSize (getWidth(), getHeight());
    setVisible (true);
    setUsingNativeTitleBar(true);
}

void MainWindow::closeButtonPressed()
{
    JUCEApplication::getInstance()->systemRequestedQuit();
}

void MainWindow::loadScript(const juce::File &file)
{
    if (!file.existsAsFile())
        return;
    if (m_engine->loadScript(file))
        MidiScripterApp::getApp().settings->recentFiles.addFile(file);
}

void MainWindow::reloadScript()
{
    m_engine->reloadScript();
}

void MainWindow::buildScript()
{
    m_engine->buildScript();
}

void MainWindow::runScript()
{
    m_engine->runScript();
}

void MainWindow::stopScript()
{
}

void MainWindow::toggleDebug()
{
    bool state = m_engine->getDebugger()->isEnabled();
    m_engine->getDebugger()->setEnabled(!state);
}

//==============================================================================

ApplicationCommandTarget* MainWindow::getNextCommandTarget()
{
    return nullptr;
}

void MainWindow::getAllCommands(Array<CommandID> &commands)
{
    const CommandID ids [] = {  CommandIDs::reloadScript,
                                CommandIDs::toggleDebug,
                                CommandIDs::buildScript,
                                CommandIDs::runScript,
                                CommandIDs::stopScript,
                                };
    commands.addArray(ids, numElementsInArray(ids));
}

void MainWindow::getCommandInfo(CommandID commandID, ApplicationCommandInfo &result)
{
    switch (commandID) {
        case CommandIDs::reloadScript:
            result.setInfo("Reload", "Reloads the current script.", CommandCategories::general, 0);
            result.setActive(m_engine->hasScriptLoaded());
            result.defaultKeypresses.add (KeyPress ('r', ModifierKeys::commandModifier | ModifierKeys::shiftModifier, 0));
            break;
        
        case CommandIDs::buildScript:
            result.setInfo("Build Script", "Builds the current script.", CommandCategories::general, 0);
            result.setActive(m_engine->canBuildScript());
            result.defaultKeypresses.add (KeyPress ('b', ModifierKeys::commandModifier, 0));
            break;
            
        case CommandIDs::runScript:
            result.setInfo("Run Script", "Runs the loaded script", CommandCategories::general, 0);
            result.setActive(m_engine->canRunScript());
            result.defaultKeypresses.add (KeyPress ('r', ModifierKeys::commandModifier, 0));
            break;
            
        case CommandIDs::stopScript:
            result.setInfo("Stop Script", "Stops the script's execution", CommandCategories::general, 0);
            break;
            
        case CommandIDs::toggleDebug:
            result.setInfo("Enable Debug", "Enables the debugger.", CommandCategories::debugging, 0);
            result.setTicked(m_engine->getDebugger()->isEnabled());
            break;
            
        default:
            break;
    }
}

bool MainWindow::perform(const InvocationInfo &info)
{
    switch (info.commandID)
    {
        case CommandIDs::reloadScript:              reloadScript(); break;
        case CommandIDs::buildScript:               buildScript(); break;
        case CommandIDs::runScript:                 runScript(); break;
        case CommandIDs::stopScript:                stopScript(); break;
        case CommandIDs::toggleDebug:               toggleDebug(); break;
        default:                                    break;
    }
    return true;
}