//
//  DebugTextBox.h
//  MaoJ
//
//  Created by James Mazur on 11/9/13.
//
//

#ifndef __MaoJ__DebugTextBox__
#define __MaoJ__DebugTextBox__

#include "../JuceLibraryCode/JuceHeader.h"
#include "../AngelScript/AngelScriptEngineLogger.h"

class AngelScriptEngine;

class DebugConsole : public Component, public AngelScriptEngineEventListener
{
    
public:
    DebugConsole();
    ~DebugConsole() {};
    
    void handleNewEvent(AngelScriptEngineEvent event);
    void clearText();
    
    void buttonClicked(juce::Button *);
    
    void paint (Graphics &g);
    void redraw();
    void resized();

private:
    void drawInfoLine(const String& text);
    void drawErrorLine(const String& text);
    void drawWarningLine(const String& text);

    CriticalSection m_messagesCriticalSection;
    ScopedPointer<TextEditor> m_textView;
    ScopedPointer<TextButton> m_reloadScriptButton;
    ScopedPointer<TextButton> m_loadScriptButton;
    ScopedPointer<TextButton> m_clearlogButton;
};

File getScriptToLoad();

#endif /* defined(__MaoJ__DebugTextBox__) */
