//
//  DebugTextBox.cpp
//  MaoJ
//
//  Created by James Mazur on 11/9/13.
//
//

#include "DebugTextBox.h"
#include "../AngelScript/AngelScriptEngine.h"
#include "../Application.h"

// TODO: add timestamps option for output

DebugConsole::DebugConsole()
:
AngelScriptEngineEventListener(MidiScripterApp::getApp().getAngelScriptEngine().getLogger())
{
    m_textView = new TextEditor;
    addAndMakeVisible(m_textView);
    m_textView->setMultiLine(true);
    m_textView->setReadOnly(true);
    m_textView->setCaretVisible(false);
    
    m_reloadScriptButton = new TextButton("Reload");
    addAndMakeVisible(m_reloadScriptButton);
    m_reloadScriptButton->setCommandToTrigger(&MidiScripterApp::getCommandManager(), CommandIDs::reloadScript, true);
    
    m_loadScriptButton = new TextButton("Load");
    addAndMakeVisible(m_loadScriptButton);
    m_loadScriptButton->setCommandToTrigger(&MidiScripterApp::getCommandManager(), CommandIDs::open, true);
    
    m_clearlogButton = new TextButton("Clear");
    addAndMakeVisible(m_clearlogButton);
}

void DebugConsole::handleNewEvent(AngelScriptEngineEvent event)
{
    switch (event.getSeverity()) {
        case Error:
            drawErrorLine(String(event.getMessage() ));
            break;
        case Warning:
            drawWarningLine(event.getMessage());
            break;
        case Info:
            drawInfoLine(event.getMessage());
            break;
        default:
            break;
    }
}

void DebugConsole::redraw()
{
}

void DebugConsole::paint(juce::Graphics &g)
{
}

void DebugConsole::resized()
{
    m_textView->setBounds(10, 10, getWidth()-20, getHeight()-50);
    m_reloadScriptButton->setBounds(10, getHeight()-30, 60, 20);
    m_loadScriptButton->setBounds(80, getHeight()-30, 60, 20);
    m_clearlogButton->setBounds(getWidth()-70, getHeight()-30, 60, 20);
}

void DebugConsole::drawInfoLine(const juce::String &text)
{
    const MessageManagerLock mmLock;
    m_textView->moveCaretToEnd();
    m_textView->setFont(Font(14.0f,Font::FontStyleFlags::italic));
    m_textView->setColour(TextEditor::ColourIds::textColourId, juce::Colours::blue);
    m_textView->insertTextAtCaret("INFO: ");
    m_textView->setFont(Font(14.0f,Font::FontStyleFlags::plain));
    m_textView->setColour(TextEditor::ColourIds::textColourId, juce::Colours::black);
    
    m_textView->insertTextAtCaret(text + "\n");
}

void DebugConsole::drawWarningLine(const juce::String &text)
{
    const MessageManagerLock mmLock;
    m_textView->moveCaretToEnd();
    m_textView->setFont(Font(14.0f,Font::FontStyleFlags::bold));
    m_textView->setColour(TextEditor::ColourIds::textColourId, juce::Colours::orange);
    m_textView->insertTextAtCaret("WARNING: ");
    m_textView->setFont(Font(14.0f,Font::FontStyleFlags::plain));
    m_textView->setColour(TextEditor::ColourIds::textColourId, juce::Colours::black);
    
    m_textView->insertTextAtCaret(text + "\n");
}

void DebugConsole::drawErrorLine(const juce::String &text)
{
    const MessageManagerLock mmLock;
    m_textView->moveCaretToEnd();
    m_textView->setFont(Font(14.0f,Font::FontStyleFlags::bold));
    m_textView->setColour(TextEditor::ColourIds::textColourId, juce::Colours::red);
    m_textView->insertTextAtCaret("ERROR: ");
    m_textView->setFont(Font(14.0f,Font::FontStyleFlags::plain));
    m_textView->setColour(TextEditor::ColourIds::textColourId, juce::Colours::black);
    
    m_textView->insertTextAtCaret(text + "\n");
}

void DebugConsole::buttonClicked(juce::Button *button)
{
    const MessageManagerLock mmLock;
    if (button == m_reloadScriptButton) {
    }
    else if (button == m_loadScriptButton)
    {
        m_textView->clear();
        
        MidiScripterApp::getApp().askUserToOpenFile();
    }
    else if (button == m_clearlogButton)
    {
        clearText();
    }
    
}

void DebugConsole::clearText()
{
    const MessageManagerLock mmLock;
    m_textView->clear();
}

File getScriptToLoad()
{
    File file;
    
    FileChooser myChooser ("Please select script to run", File::getSpecialLocation (File::userDesktopDirectory), "*.as");
    if (myChooser.browseForFileToOpen())
            file = myChooser.getResult();
    
    return file;
}

