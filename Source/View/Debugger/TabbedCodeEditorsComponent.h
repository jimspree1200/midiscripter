//
//  TabbedCodeEditorsComponent.h
//  MaoJ
//
//  Created by Jim Mazur on 12/31/13.
//
//

#ifndef __MaoJ__TabbedCodeEditorsComponent__
#define __MaoJ__TabbedCodeEditorsComponent__

#include <iostream>
#include "../../../JuceLibraryCode/JuceHeader.h"
#include <map>
#include "ScriptCodeViewer.h"

class DebugViewController;

class EmptyComponent : public Component
{
public:
    EmptyComponent() {}
    
    void resized()
    {
        setBounds(getBoundsInParent());
    }
    
    void paint (Graphics& g)
    {
        
    }
    
};

class TabbedScriptViewer :  public Component,
                            public ChangeListener
{
    
public:
    TabbedScriptViewer();
    
    ~TabbedScriptViewer();
    
    void changeListenerCallback(ChangeBroadcaster *source);
    
    void addScripts(Array<File> scripts);
    
    void addScript(File script);
    
    Array<File> getAllScripts();
    
    void removeAllScripts();
    
    bool isEmpty();
    
    void setScriptFocus(File script, int lineNumber);
    
    void resized() override;
    
    void paint (Graphics& g) override;
    
private:
    ScopedPointer<TabbedComponent> m_tabbedComponent;
    OwnedArray<ScriptCodeViewer> m_codeViewers;
    CPlusPlusCodeTokeniser tokeniser;
    Array<CodeDocument> m_documents;
    
    ScriptCodeViewer* findViewerThatContains(File fileToLookFor);
    void setActiveTab(String name);
    bool containsScript(File script);
    void clearAllScripts();
};

#endif /* defined(__MaoJ__TabbedCodeEditorsComponent__) */
