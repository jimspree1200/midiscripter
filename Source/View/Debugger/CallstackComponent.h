//
//  CallstackComponent.h
//  MaoJ
//
//  Created by Jim Mazur on 12/26/13.
//
//

#ifndef __MaoJ__CallstackComponent__
#define __MaoJ__CallstackComponent__

#include "../JuceLibraryCode/JuceHeader.h"

class AngelScriptEngine;

class CallstackTreeItem : public TreeViewItem
{
public:
    CallstackTreeItem(ValueTree& refTree);
    //~CallstackTreeItem(){};
    
    bool mightContainSubItems() override;
    void itemOpennessChanged(bool isNowOpen) override;
    
    int getItemHeight() const override { return 15; };
    void paintItem(juce::Graphics &g, int width, int height) override;
    void refreshSubItems();
private:
    AttributedString prepareVariableString();
    AttributedString prepareStackString();
    ValueTree tree;
};

//==============================================================================

class CallstackTree : public Component,
                      public ChangeListener
{
    
    
public:
    CallstackTree();
    ~CallstackTree();
    
    void updateTree(ValueTree newTree);
    
    void paint(juce::Graphics &g) override;
    void resized() override;
    
    void changeListenerCallback(juce::ChangeBroadcaster *source) override;
    
private:
    TreeView m_tree;
    ScopedPointer<CallstackTreeItem> rootItem;
    ValueTree m_valueTree;
};

#endif /* defined(__MaoJ__CallstackComponent__) */
