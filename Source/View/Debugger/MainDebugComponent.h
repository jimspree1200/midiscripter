//
//  MainDebugComponent.h
//  MaoJ
//
//  Created by Jim Mazur on 12/27/13.
//
//

#ifndef __MaoJ__MainDebugComponent__
#define __MaoJ__MainDebugComponent__

#include "../JuceLibraryCode/JuceHeader.h"
#include "CallstackComponent.h"
#include "ScriptExecutionButtonsComponent.h"
#include "TabbedCodeEditorsComponent.h"
#include "../DebugTextBox.h"

class AngelScriptEngine;


class MainDebugComponent : public Component,
                           public ApplicationCommandTarget
{
    
public:
    MainDebugComponent();
    //~MainDebugComponent();
    
    //==============================================================================
    ApplicationCommandTarget* getNextCommandTarget() override;
    void getAllCommands(Array<CommandID> &commands) override;
    void getCommandInfo (CommandID commandID, ApplicationCommandInfo& result);
    bool perform (const InvocationInfo& info) override;
    
    void resized();
    
private:
    AngelScriptEngine* m_angelScriptEngine = nullptr;
    
    StretchableLayoutManager m_layoutManager;
    StretchableLayoutResizerBar m_layoutResizerBar;
    
    ScopedPointer<TabbedScriptViewer> m_TabbedScriptView;
    ScopedPointer<TabbedComponent> m_TabbedCallstackAndBreakpoints;
    ScopedPointer<CallstackTree> m_callstackTree;
    ScopedPointer<ScriptExecutionButtonsComponent> m_scriptExecutionButtons;
    ScopedPointer<DebugConsole> m_console;
    
};

#endif /* defined(__MaoJ__MainDebugComponent__) */
