//
//  ScriptCodeViewer.cpp
//  MaoJ
//
//  Created by Jim Mazur on 1/2/14.
//
//

#include "ScriptCodeViewer.h"
#include "../../Application.h"

ScriptCodeViewer::ScriptCodeViewer (File script)
:
m_scriptFile(script)
{
    // Must be a valid file
    jassert(script.existsAsFile());
    
    FileInputStream stream(script);
    m_doc = new CodeDocument;
    m_doc->replaceAllContent(stream.readEntireStreamAsString());
    
    m_codeEditor = new CodeDebugComponent(*m_doc, &m_tokeniser);
    
    BreakPointsContainer* const container = MidiScripterApp::getAngelScriptEngine().getDebugger()->getBreakPointsManager()->getContainerFor(script);
    jassert(container != nullptr);
    
    m_codeEditor->setBreakpointsContainer(container);
    
    addAndMakeVisible(m_codeEditor);
}

void ScriptCodeViewer::moveToLineNumber(int lineNumber)
{
    CodeDocument::Position position (*m_doc, lineNumber-1, 0);
    m_codeEditor->moveCaretTo(position, false);
    m_codeEditor->setExecutingLine(lineNumber);
}

void ScriptCodeViewer::resized()
{
    m_codeEditor->setBounds(getLocalBounds().reduced(2));
}