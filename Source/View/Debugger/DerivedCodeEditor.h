//
//  DerivedCodeEditor.h
//  MidiScripter
//
//  Created by Jim Mazur on 4/5/14.
//
//

#ifndef __MidiScripter__DerivedCodeEditor__
#define __MidiScripter__DerivedCodeEditor__

#include <iostream>
#include "JuceHeader.h"

class BreakPointsContainer;

/** A modified version of JUCE's CodeEditorComponent
 
 To create one of these simply use the CodeEditor as you normally
 would, however, pass along the breakpoint manager to handle the
 break points. The breakpoint file will automatically update this
 components view when a change is made.
 
 This class is more a less a hack, overriding away the paint function
 from the CodeEditorComponent and then inserting its own painting function
 for the breakpoint gutter and background for the showing the active code
 line. Finally it calls the CodeEditorComponet draw function to draw
 on top of all the break points and markings in editor area. In order to
 show anything, the backgroundColourId of the CodeEditorComponent must be
 transparent.
 
 More so, the mouse events are also intercepted by overiding the mouseDown,
 mouseMove and mouseExit functions.
 
 Therefore its important to modify the CodeEditorComponent's private members
 to be protected upon updating the JUCE modules.
 
 @see juce::CodeEditorComponent
 */

class CodeDebugComponent : public CodeEditorComponent,
private ValueTree::Listener
{
public:
    
    CodeDebugComponent(CodeDocument& document,
                       CodeTokeniser* codeTokeniser);
    
    void setBreakpointsContainer(BreakPointsContainer* container);
    
    void setExecutingLine (int line);
    
    void paint(Graphics &g) override;
    
    void mouseDown(const MouseEvent &event);
    
    void mouseMove(const MouseEvent &event);
    
    void mouseExit(const MouseEvent &event);
    
    enum ColourIds
    {
        executingLineBackgorundId   = 0x1005500,  /**< TheColour used for the background of the executing line */
        gutterHoverOverId            = 0x1005501,  /**< The colour to use when the mouse hovers over a line in the gutter. */
        breakpointUnactive           = 0x1005502,  /**< The colour to use when a break point is unactive. */
        breakpointActive             = 0x1005503   /**< The colour to use when a break point is active. */
    };
    
private:
    
    BreakPointsContainer* m_breakPointContainer = nullptr;
    
    int m_lastMouseOverLine;
    
    int m_currentExecutingLine = 0;
    
    /** Paints the break points under the gutter component.
     */
    void paintBreakpoints(Graphics &g);
    
    /** Paints the mouse over state under the gutter component.
     */
    void paintGutterMouseOver(Graphics &g);
    
    /** Highlights a line to mark it as the current executing line.
     */
    void paintExecutingLine (Graphics &g);
    
    /** Repaints the entire view when the breakpoint ValueTree changes.
     */
    void valueTreePropertyChanged (ValueTree& treeWhosePropertyHasChanged,
                                   const Identifier& property) override         { repaint(); };

void valueTreeChildAdded (ValueTree& parentTree,
                          ValueTree& childWhichHasBeenAdded) override       { repaint(); };

void valueTreeChildRemoved (ValueTree& parentTree, ValueTree&) override     { repaint(); };

void valueTreeChildOrderChanged (ValueTree& parentTree) override            {};

void valueTreeParentChanged (ValueTree&) override                           {};

void treeChildrenChanged (const ValueTree& parentTree)                      {}
};



#endif /* defined(__MidiScripter__DerivedCodeEditor__) */
