//
//  ScriptExecutionButtonsComponent.cpp
//  MaoJ
//
//  Created by Jim Mazur on 12/27/13.
//
//

#include "ScriptExecutionButtonsComponent.h"
#include "../../Application.h"

ScriptExecutionButtonsComponent::ScriptExecutionButtonsComponent()
{
    
    ApplicationCommandManager* cm = &MidiScripterApp::getCommandManager();
    
    addAndMakeVisible(m_toggleBreakpointsButton = new TextButton("Toggle Breakpoints"));
    m_toggleBreakpointsButton->setConnectedEdges(Button::ConnectedOnRight);
    m_toggleBreakpointsButton->setCommandToTrigger(cm, CommandIDs::toggleBreakpoints, true);
    
    addAndMakeVisible(m_pauseAndContinueButton = new TextButton("Pause/Continue"));
    m_pauseAndContinueButton->setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    m_pauseAndContinueButton->setCommandToTrigger(cm, CommandIDs::pauseContinueExecution, true);
    
    addAndMakeVisible(m_stepOverButton = new TextButton("Step Over"));
    m_stepOverButton->setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    m_stepOverButton->setCommandToTrigger(cm, CommandIDs::stepOver, true);;
    
    addAndMakeVisible(m_stepIntoButton = new TextButton("Step Into"));
    m_stepIntoButton->setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    m_stepIntoButton->setCommandToTrigger(cm, CommandIDs::stepInto, true);;
    
    addAndMakeVisible(m_stepOutButton = new TextButton("Step Out"));
    m_stepOutButton->setConnectedEdges(Button::ConnectedOnLeft);
    m_stepOutButton->setCommandToTrigger(cm, CommandIDs::stepOut, true);
    
    for (int i = 0; i < 5; i++) {
        m_layoutManager.setItemLayout(i, 90, 100, 30);
    }
}

void ScriptExecutionButtonsComponent::resized()
{
    Rectangle<int> b = getLocalBounds().reduced(3);
    Component* comps[] = { m_toggleBreakpointsButton, m_pauseAndContinueButton, m_stepOverButton, m_stepIntoButton, m_stepOutButton };
    m_layoutManager.layOutComponents(comps, 5, b.getX(), b.getY(), getWidth()*.30, b.getHeight(), false, true);
}