//
//  CallstackComponent.cpp
//  MaoJ
//
//  Created by Jim Mazur on 12/26/13.
//
//

#include "CallstackComponent.h"
#include "../../Application.h"

CallstackTreeItem::CallstackTreeItem(ValueTree& refTree)
:
tree(refTree)
{
}

bool CallstackTreeItem::mightContainSubItems()
{
    return tree.getNumChildren() > 0;
}

void CallstackTreeItem::itemOpennessChanged(bool isNowOpen)
{
    if (isNowOpen && getNumSubItems() == 0)
        refreshSubItems();
    else
        clearSubItems();
}

void CallstackTreeItem::paintItem(juce::Graphics &g, int width, int height)
{
    AttributedString as;
    
    if (tree.getType().toString() == "Variable")
        as = prepareVariableString();
    
    else if (tree.getType().toString() == "StackLevel")
        as = prepareStackString();
    else
        as.append("Unknown Entity");
    
    Rectangle<int> drawArea = g.getClipBounds().withTrimmedLeft(4);
    
    as.draw(g, drawArea.toFloat());
}

void CallstackTreeItem::refreshSubItems()
{
    clearSubItems();
    for (int i = 0; i < tree.getNumChildren(); i++)
    {
        
        // We don't want to add the 'Variables' node as its redundant
        if (tree.getChild(i).getType().toString() == "Variables")
        {
            ValueTree variables = tree.getChild(i);
            for (int j = 0; j < variables.getNumChildren(); j++)
            {
                ValueTree variable = variables.getChild(j);
                addSubItem(new CallstackTreeItem(variable));
            }
            return;
        }
        else
        {
            ValueTree treeToAdd = tree.getChild(i);
            addSubItem(new CallstackTreeItem( treeToAdd ));
        }
        
    }
}

AttributedString CallstackTreeItem::prepareVariableString()
{
    AttributedString aString;
    
    aString.append(tree.getProperty("Name").toString() + " = ", Font().boldened());
    
    aString.append("("
                   + tree.getProperty("TypeNamed").toString()
                   + ") ", Colours::grey);
    
    if (!tree.getProperty("IsInScope"))
        aString.append("Not in scope", Font().italicised(), Colours::blue);
    else
        aString.append(tree.getProperty("Value").toString());
    
    return aString;
}

AttributedString CallstackTreeItem::prepareStackString()
{
    AttributedString aString;

    aString.append(tree.getProperty("LevelNum").toString()
                   + " "
                   + tree.getProperty("StackFunctionDeclaration").toString());
    
    return aString;
}

//==============================================================================

CallstackTree::CallstackTree()
{
    addAndMakeVisible(&m_tree);
    m_tree.setIndentSize(10);
    m_tree.setDefaultOpenness (false);
    m_tree.setMultiSelectEnabled (false);
    m_tree.setRootItemVisible(false);
    m_tree.setRootItem(rootItem = new CallstackTreeItem(m_valueTree));
    
    MidiScripterApp::getAngelScriptEngine().getDebugger()->getStackManager()->addChangeListener(this);
}

CallstackTree::~CallstackTree()
{
    m_tree.setRootItem(nullptr);
    
    MidiScripterApp::getAngelScriptEngine().getDebugger()->getStackManager()->removeChangeListener(this);
}

void CallstackTree::paint(juce::Graphics &g)
{
    g.fillAll (Colours::white);
    
    if (!m_valueTree.isValid())
    {
        g.setFont (Font (16.0f));
        g.setColour (Colours::black);
        g.drawText ("No Debugger Running", getLocalBounds(), Justification::centred, true);
    }
}

void CallstackTree::updateTree(juce::ValueTree newTree)
{
    m_valueTree = newTree;
    
    m_tree.setRootItem(rootItem = new CallstackTreeItem(m_valueTree));
    rootItem->refreshSubItems();
    rootItem->treeHasChanged();
    
    if (rootItem->getNumSubItems() > 0);
        rootItem->getSubItem(rootItem->getNumSubItems()-1)->setOpen(true);
}

void CallstackTree::resized()
{
    Rectangle<int> r (getLocalBounds().reduced (3));
    m_tree.setBounds(r);
}

void CallstackTree::changeListenerCallback(juce::ChangeBroadcaster *source)
{
    StackManager* const sm = MidiScripterApp::getAngelScriptEngine().getDebugger()->getStackManager();
    
    if (source != sm)
        return;
    
    updateTree(sm->getStackTree());
}
