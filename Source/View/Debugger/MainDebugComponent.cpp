//
//  MainDebugComponent.cpp
//  MaoJ
//
//  Created by Jim Mazur on 12/27/13.
//
//

#include "MainDebugComponent.h"
#include "../../AngelScript/AngelScriptEngine.h"
#include "../../Application.h"

MainDebugComponent::MainDebugComponent()
:
m_layoutResizerBar(&m_layoutManager, 1, true)
{
    m_angelScriptEngine = &MidiScripterApp::getApp().getAngelScriptEngine();
    
    ApplicationCommandManager* const cm = &MidiScripterApp::getCommandManager();
    cm->registerAllCommandsForTarget(this);
    
    addAndMakeVisible(m_TabbedScriptView = new TabbedScriptViewer);
    addAndMakeVisible(m_TabbedCallstackAndBreakpoints = new TabbedComponent(TabbedButtonBar::Orientation::TabsAtTop));
    
    addAndMakeVisible(&m_layoutResizerBar);
    
    addAndMakeVisible(m_scriptExecutionButtons = new ScriptExecutionButtonsComponent());
    
    addAndMakeVisible(m_console = new DebugConsole);
    
    m_TabbedCallstackAndBreakpoints->setTabBarDepth(30);
    m_TabbedCallstackAndBreakpoints->addTab("Callstack", Colours::lightgrey.darker(), m_callstackTree = new CallstackTree(), false);
    
    m_layoutManager.setItemLayout(0, -.10, -.90, -.60);
    m_layoutManager.setItemLayout(1, 2, 2, 2);
    m_layoutManager.setItemLayout(2, -.10, -.50, -.40);
    
}

ApplicationCommandTarget* MainDebugComponent::getNextCommandTarget()
{
    return findFirstTargetParentComponent();
}

void MainDebugComponent::getAllCommands(Array<CommandID> &commands)
{
    const CommandID ids [] = {
        CommandIDs::toggleBreakpoints,
        CommandIDs::pauseContinueExecution,
        CommandIDs::stepInto,
        CommandIDs::stepOut,
        CommandIDs::stepOver };
    commands.addArray(ids, numElementsInArray(ids));
}

void MainDebugComponent::getCommandInfo(CommandID commandID, ApplicationCommandInfo &result)
{
    AngelScriptDebugger* debugger = MidiScripterApp::getAngelScriptEngine().getDebugger();
    
    switch (commandID) {
        case CommandIDs::toggleBreakpoints:
            result.setInfo("Toggle Breakpoints", "Enables and disables the breakpoints", CommandCategories::debugging, 0);
            //needs connections
            result.setActive(false);
            break;
            
        case CommandIDs::pauseContinueExecution:
            result.setInfo("Pause and Continue", "Pauses and resumes execution flow of the script", CommandCategories::debugging, 0);
            result.defaultKeypresses.add (KeyPress (KeyPress::spaceKey, ModifierKeys::altModifier, 0));
            result.setActive(debugger->isWaitingForCommand());
            break;
        
        case CommandIDs::stepInto:
            result.setInfo("Step Into", "Step into the current function.", CommandCategories::debugging, 0);
            result.defaultKeypresses.add (KeyPress (KeyPress::rightKey, ModifierKeys::altModifier, 0));
            result.setActive(debugger->isWaitingForCommand());
            break;
            
        case CommandIDs::stepOut:
            result.setInfo("Step Out", "Step out of the current function", CommandCategories::debugging, 0);
            result.defaultKeypresses.add (KeyPress (KeyPress::leftKey, ModifierKeys::altModifier, 0));
            result.setActive(debugger->isWaitingForCommand());
            break;
            
        case CommandIDs::stepOver:
            result.setInfo("Step Over", "Step over the current function.", CommandCategories::debugging, 0);
            result.defaultKeypresses.add (KeyPress (KeyPress::downKey, ModifierKeys::altModifier, 0));
            result.setActive(debugger->isWaitingForCommand());
            break;
            
        default:
            break;
    }
}

bool MainDebugComponent::perform(const InvocationInfo &info)
{
    AngelScriptDebugger* debugger = MidiScripterApp::getAngelScriptEngine().getDebugger();
    
    switch (info.commandID) {
        case CommandIDs::toggleBreakpoints:
            // do something here!
            break;
            
        case CommandIDs::pauseContinueExecution:
            if ( debugger->isWaitingForCommand() )
                debugger->setDebuggerAction(AngelScriptDebugger::DebugAction::Continue);
            else
                debugger->setDebuggerAction(AngelScriptDebugger::DebugAction::Pause);
            break;
            
        case CommandIDs::stepInto:
           debugger->setDebuggerAction(AngelScriptDebugger::DebugAction::Step_Into);
            break;
            
        case CommandIDs::stepOut:
            debugger->setDebuggerAction(AngelScriptDebugger::DebugAction::Step_Out);
            break;
            
        case CommandIDs::stepOver:
            debugger->setDebuggerAction(AngelScriptDebugger::DebugAction::Step_Over);
            break;
            
        default:
            return false;
    }
    
    return true;
}

void MainDebugComponent::resized()
{
    Rectangle<int> bounds = getLocalBounds().reduced(4);
    Rectangle<int> loggerBounds = bounds.removeFromBottom(140);
    Rectangle<int> buttonBounds = loggerBounds.removeFromTop(30);
    
    
    Component* comps[] = { m_TabbedScriptView, &m_layoutResizerBar, m_TabbedCallstackAndBreakpoints };
    m_layoutManager.layOutComponents(comps, 3, bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight(), false, true);
    
    m_scriptExecutionButtons->setBounds(buttonBounds);
    m_console->setBounds(loggerBounds);
}
