//
//  DerivedCodeEditor.cpp
//  MidiScripter
//
//  Created by Jim Mazur on 4/5/14.
//
//

#include "DerivedCodeEditor.h"
#include "../../AngelScript/BreakPointsContainer.h"

class CodeEditorComponent::GutterComponent  : public Component
{
};

CodeDebugComponent::CodeDebugComponent(CodeDocument& document,
                                       CodeTokeniser* codeTokeniser)
:
CodeEditorComponent(document, codeTokeniser)
{
    // Required in order to make the below paint method work
    setColour(CodeEditorComponent::ColourIds::backgroundColourId, Colour().withAlpha(0.0f));
    setReadOnly(true);
    
    //  IMPORTANT: Getting a build error here with 'private member' warning means you have probably updated JUCE. To fix the issue, make the private members of CodeEditorComponent proctected.
    
    removeChildComponent(caret);
    
    // Catch events from the gutter component as it is created from within the CodeEditorComponent and will not automatically callback to this class
    gutter->addMouseListener(this, false);
    
    setColour(ColourIds::executingLineBackgorundId, Colour(0xffd5f3cc));
    setColour(ColourIds::breakpointActive, Colour(0xff61aaf9));
    setColour(ColourIds::breakpointUnactive, Colour(0xffdaecff));
    setColour(ColourIds::gutterHoverOverId, Colour(0xffebebeb));
    
}

void CodeDebugComponent::setExecutingLine(int line)
{
    m_currentExecutingLine = line;
    repaint();
}

void CodeDebugComponent::setBreakpointsContainer(BreakPointsContainer *container)
{
    m_breakPointContainer = container;
    m_breakPointContainer->getBreakpointsTree().addListener(this);
}

void CodeDebugComponent::paint(juce::Graphics &g)
{
    /* This is kind of a hack, overriding the paint method and drawing behind the component before calling the CodeEditor paint function. By setting the backgroundColourId to transparent we can inject the below into the component and not have it immediately wiped out by the CodeEditorComponent's paint implemenation.
     */
    
    // Erase any old content
    g.setColour(Colours::white);
    g.fillRect(getLocalBounds());
    
    paintExecutingLine(g);
    
    paintGutterMouseOver(g);
    
    if (m_breakPointContainer != nullptr) {
        paintBreakpoints(g);
    }
    
    CodeEditorComponent::paint(g);
    
    
}

void CodeDebugComponent::mouseDown(const juce::MouseEvent &event)
{
    DBG(event.getMouseDownX());
    if (event.eventComponent == gutter && m_breakPointContainer != nullptr)
    {
        int lineclicked = event.getPosition().getY() / lineHeight + firstLineOnScreen + 1;
        if (m_breakPointContainer->hasBreakPoint(lineclicked))
        {
            if (event.mods.isCommandDown() || event.mods.isRightButtonDown())
                m_breakPointContainer->removeBreakpoint(lineclicked);
            else
                m_breakPointContainer->toggleBreakPoint(lineclicked);
        }
        else
            m_breakPointContainer->addBreakpoint(lineclicked);
    }
    
    CodeEditorComponent::mouseDown(event);
}

void CodeDebugComponent::mouseMove(const juce::MouseEvent &event)
{
    if (event.eventComponent == gutter) {
        int currentMouseOverLine = event.getPosition().getY() / lineHeight;
        if (currentMouseOverLine == m_lastMouseOverLine)
            return;
        m_lastMouseOverLine = currentMouseOverLine;
        repaint();
    }
}

void CodeDebugComponent::mouseExit(const juce::MouseEvent &event)
{
    if (event.eventComponent == gutter)
        // Need to reset this otherwise gutter may not potentailly refresh when re-entering
        m_lastMouseOverLine = -1;
    repaint();
}

void CodeDebugComponent::paintBreakpoints(juce::Graphics &g)
{
    Rectangle<float> rect = gutter->getLocalBounds().toFloat();
    rect = rect.removeFromTop(lineHeight);
    
    DrawableRectangle drawableRectangle;
    RelativeParallelogram relativeParallelogram(rect);
    drawableRectangle.setRectangle(relativeParallelogram);
    
    const Rectangle<int> clip (g.getClipBounds());
    const int firstLineToDraw = jmax (0, clip.getY() / lineHeight);
    const int lastLineToDraw = jmin (lines.size(), clip.getBottom() / lineHeight + 1,  document.getNumLines() - firstLineOnScreen);
    
    for (int i = firstLineToDraw; i < lastLineToDraw; i++)
    {
        int lineToCheck = i + firstLineOnScreen + 1;
        
        if (m_breakPointContainer->hasBreakPoint(lineToCheck))
        {
            if (m_breakPointContainer->breakpointIsActive(lineToCheck))
                drawableRectangle.setFill(FillType (findColour(ColourIds::breakpointActive)));
            
            else
                drawableRectangle.setFill(FillType (findColour(ColourIds::breakpointUnactive)));
            
            drawableRectangle.drawAt(g, 0, lineHeight * i, 1.0f);
        }
    }
}

void CodeDebugComponent::paintGutterMouseOver(juce::Graphics &g)
{
    if (gutter->isMouseOver(true))
    {
        const Rectangle<int> clip (g.getClipBounds());
        const int lastLineToDraw = jmin (lines.size(), clip.getBottom() / lineHeight + 1,  document.getNumLines() - firstLineOnScreen);
        
        if (lastLineToDraw > m_lastMouseOverLine)
        {
            g.setColour(findColour(ColourIds::gutterHoverOverId));
            g.fillRect(0, m_lastMouseOverLine * lineHeight, gutter->getWidth(), lineHeight);
        }
    }
}

void CodeDebugComponent::paintExecutingLine(juce::Graphics &g)
{
    const Rectangle<int> clip (g.getClipBounds());
    const int lastLineToDraw = jmin (lines.size(), clip.getBottom() / lineHeight + 1,  document.getNumLines() - firstLineOnScreen);
    
    if ( m_currentExecutingLine >= firstLineOnScreen + 1 &&
        m_currentExecutingLine <= lastLineToDraw + firstLineOnScreen)
    {
        g.setColour(findColour(ColourIds::executingLineBackgorundId));
        g.fillRect(gutter->getWidth(),
                   (m_currentExecutingLine - firstLineOnScreen - 1) * lineHeight,
                   getWidth(),
                   lineHeight);
    }
}

