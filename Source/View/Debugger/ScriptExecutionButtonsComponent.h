//
//  ScriptExecutionButtonsComponent.h
//  MaoJ
//
//  Created by Jim Mazur on 12/27/13.
//
//

#ifndef __MaoJ__ScriptExecutionButtonsComponent__
#define __MaoJ__ScriptExecutionButtonsComponent__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

class ScriptExecutionButtonsComponent : public Component
{
public:
    ScriptExecutionButtonsComponent();
    void resized() override;
    
private:
    StretchableLayoutManager m_layoutManager;
    ScopedPointer<TextButton> m_toggleBreakpointsButton;
    ScopedPointer<TextButton> m_pauseAndContinueButton;
    ScopedPointer<TextButton> m_stepOverButton;
    ScopedPointer<TextButton> m_stepIntoButton;
    ScopedPointer<TextButton> m_stepOutButton;
};

#endif /* defined(__MaoJ__ScriptExecutionButtonsComponent__) */
