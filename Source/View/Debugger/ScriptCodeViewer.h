//
//  ScriptCodeViewer.h
//  MaoJ
//
//  Created by Jim Mazur on 1/2/14.
//
//

#ifndef __MaoJ__ScriptCodeViewer__
#define __MaoJ__ScriptCodeViewer__

#include <iostream>
#include "JuceHeader.h"
#include "DerivedCodeEditor.h"

class DebugViewController;

class ScriptCodeViewer : public Component
{
    
public:
    ScriptCodeViewer(File script);
    
    File getScriptFile()                { return m_scriptFile; };
    bool containsScript(File script)    { return script == m_scriptFile; };
    
    void moveToLineNumber (int lineNumber);
    
    void resized() override;
    
private:
    File m_scriptFile;
    
    ScopedPointer<CodeDocument> m_doc;
    ScopedPointer<CodeDebugComponent> m_codeEditor;
    ScopedPointer<CodeDebugComponent> m_codeDebugComponent;
    CPlusPlusCodeTokeniser m_tokeniser;
    
};

#endif /* defined(__MaoJ__ScriptCodeViewer__) */
