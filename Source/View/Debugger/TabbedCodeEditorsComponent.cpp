//
//  TabbedCodeEditorsComponent.cpp
//  MaoJ
//
//  Created by Jim Mazur on 12/31/13.
//
//

#include "TabbedCodeEditorsComponent.h"
#include "../../Application.h"

TabbedScriptViewer::TabbedScriptViewer()
{
    AngelScriptEngine* const engine = &MidiScripterApp::getAngelScriptEngine();
    engine->addChangeListener(this);
    engine->getDebugger()->getStackManager()->addChangeListener(this);
    
    // In case engine was already started, get new scripts
    if (isEmpty())
        addScripts(engine->getCompiledScripts());
}

TabbedScriptViewer::~TabbedScriptViewer()
{
    AngelScriptEngine* const engine = &MidiScripterApp::getAngelScriptEngine();
    engine->removeChangeListener(this);
    engine->getDebugger()->getStackManager()->removeChangeListener(this);
}

void TabbedScriptViewer::changeListenerCallback(juce::ChangeBroadcaster *source)
{
    AngelScriptEngine* const engine = &MidiScripterApp::getAngelScriptEngine();
    StackManager* const stackManager = engine->getDebugger()->getStackManager();
    
    if (source == engine)
    {
        switch (engine->getEngineState())
        {
            case AngelScriptEngine::EngineState::Ready:
                addScripts(engine->getCompiledScripts());
                break;
                
            case AngelScriptEngine::EngineState::notCreated:
                clearAllScripts();
                break;
                
            default:
                break;
        }
    }
    else if (source == stackManager)
    {
        // Set current executing line to focus
        int line = stackManager->getStackTree().getProperty("CurrentLineNum");
        File file(stackManager->getStackTree().getProperty("CurrentFile").toString());
        setScriptFocus(file, line);
    }
    
}

void TabbedScriptViewer::addScripts(Array<juce::File> scripts)
{
    for (int i = 0; i < scripts.size(); i++)
    {
        addScript(scripts[i]);
    }
}

void TabbedScriptViewer::addScript(juce::File script)
{
    String scriptFileName = script.getFileName();
    
    if (m_tabbedComponent == nullptr)
    {
        addAndMakeVisible(m_tabbedComponent = new TabbedComponent(TabbedButtonBar::Orientation::TabsAtTop));
        resized();
    }
    
    if (!containsScript(script))
        m_tabbedComponent->addTab(scriptFileName,
                                  Colours::grey,
                                  m_codeViewers.add(new ScriptCodeViewer(script)),
                                  false);
}

Array<File> TabbedScriptViewer::getAllScripts()
{
    Array<File> files;
    
    for (int i = 0; i < m_codeViewers.size(); i++)
        files.add( m_codeViewers[i]->getScriptFile() );
    
    return files;
}

void TabbedScriptViewer::removeAllScripts()
{
    if (m_tabbedComponent != nullptr) {
        m_tabbedComponent->clearTabs();
    }
}

bool TabbedScriptViewer::isEmpty()
{
    return !getAllScripts().size();
}

void TabbedScriptViewer::setScriptFocus(File script, int lineNumber)
{
    jassert(containsScript(script));
    
    String scriptFileName = script.getFileName();
    setActiveTab(scriptFileName);
    findViewerThatContains(script)->moveToLineNumber(lineNumber);
    
}

ScriptCodeViewer* TabbedScriptViewer::findViewerThatContains(juce::File fileToLookFor)
{
    for (int i = 0; i < m_codeViewers.size(); i++)
    {
        if (m_codeViewers[i]->containsScript(fileToLookFor))
            return m_codeViewers[i];
    }
    return nullptr;
}

void TabbedScriptViewer::setActiveTab(juce::String name)
{
    StringArray tabNames = m_tabbedComponent->getTabNames();
    for (int i = 0; i < tabNames.size(); i++)
    {
        if (tabNames[i] != name)
            continue;
        m_tabbedComponent->setCurrentTabIndex(i);
        break;
    }
}

bool TabbedScriptViewer::containsScript(File script)
{
    if (m_codeViewers.size() == 0)
        return false;
    
    for (auto viewer : m_codeViewers)
    {
        if (script == viewer->getScriptFile())
            return true;
    }
    
    return false;
}

void TabbedScriptViewer::resized()
{
    if (m_tabbedComponent != nullptr)
        m_tabbedComponent->setBounds(getLocalBounds());
}

void TabbedScriptViewer::paint(Graphics &g)
{
    g.setFont (Font (16.0f));
    g.setColour (Colours::black);
    g.drawText ("No Compiled Script", getLocalBounds(), Justification::centred, true);
}

void TabbedScriptViewer::clearAllScripts()
{
    if (m_tabbedComponent == nullptr)
        return;
    
    m_tabbedComponent->clearTabs();
    m_tabbedComponent = nullptr;
}