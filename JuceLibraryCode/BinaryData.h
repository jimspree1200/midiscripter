/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#ifndef BINARYDATA_H_48569114_INCLUDED
#define BINARYDATA_H_48569114_INCLUDED

namespace BinaryData
{
    extern const char*   as_callfunc_arm_msvc_asm;
    const int            as_callfunc_arm_msvc_asmSize = 6942;

    extern const char*   as_callfunc_x64_msvc_asm_asm;
    const int            as_callfunc_x64_msvc_asm_asmSize = 5041;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Number of elements in the namedResourceList array.
    const int namedResourceListSize = 2;

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes) throw();
}

#endif
