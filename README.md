# MidiScripter Project #

## Why does this project exist? ##
I created this project in retrospect to my own personal journey in trying to learn C++. 

I have spent many years working with and creating Midi controllers and in that process I've been confronted with the need to inject logic in the controllers at some stage. In some cases the controllers did not yet have feature complete firmware, so in order to test the controllers in a real use case, there was logic missing. In other cases, I was prototyping workflows and functionality to test user experience before defining a specification for development teams.

My go to tool in most all circumstances was Bome's MIDI translator. Which is a fantastic tool for creating logic and presets for controllers. If you are landing upon this page because you want to do some basic MIDI programming, go no further, that tool will accommodate 90% of the typical scenarios for injecting MIDI logic.

However, in my own personal journey, I realized at one point that I must learn actual programming in order to achieve more advanced prototypes. This triggered my journey into trying to learn C++... (to be continued)

This project serves two primary purposes:

* An object orientated approach towards Midi programming. Allowing scripts to be quickly loaded and run on any Mac or PC computer.
* An entrance point for Midi enthusiasts who desire to learn C++ programming, without the initial upfront battles of getting an IDE setup (Xcode of MSVS) and figuring out how to use a Midi I/O library (PortMidi, JUCE or native).

## What is the application? ##
The application provides a runtime to execute scripts created in the AngelScript language. The language is very close to C++ and code written in AngelScript should be able to be ported to actual C++ code without much effort. Assumption being that any coder using this project will eventually want to move on to actually using C++. 

The application uses the JUCE framework for GUI, events, timers, and most importantly Midi I/O. The application creates bindings so the scripts can easily deal with Midi events, opening Midi ports, and in the case of MacOS, actually creating ports.